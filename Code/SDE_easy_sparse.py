import numpy as np
import scipy as sp
from scipy.sparse import kronsum, kron, csr_matrix
from scipy import *
import time
#from scipy.linalg import kron
from matplotlib import pyplot as plt

def F1(a, A, B, x):
    z = dot( x, A.dot(x) );
    y = a*(2*z* A.dot(x) -  B.dot(A.dot(x)) -z*z * x)
    return y
    
def F2(a,A, x):
     z = dot( x, A.dot(x) );
     y = np.sqrt(2*a)*(A.dot(x) - z*x)
     return y
     
def ScalInt(k, t):
    '''computes the scalar part of the integrand of the 
    integral (20) HLS'''
    return k*np.exp(-2*k*t)*(np.exp(k*t) -1)

def Nph(Jm, Jp, psiArr, t, k, g, Nsteps, tMax, dt):
    #determine photon density using eq. 20 from the paper
    #as well as trapezoidal rule for quadrature
    res = 0
    for i in range(1, Nsteps-1):	#stupid offsetting range(1,n)=[1,...,n-1]
        psi = psiArr[Nsteps-1 -i]
        fa = dot(psi, (Jp.dot(Jm) ).dot(psi))
        res+=   ScalInt(k,t[i]) *fa 
    res *=2
    
    #first point
    psi = psiArr[Nsteps-1]
    fa = dot(psi, (Jp.dot(Jm) ).dot(psi))
    res += ScalInt(k, 0) * fa
    #last point
    psi = psiArr[0]
    fa = dot(psi, (Jp.dot(Jm) ).dot(psi))
    res += ScalInt(k, t[Nsteps-1]) * fa
    res *= dt/2
    res *= 2 * (g/k)**2       #FORGOT THE SCALING!!!
    return res

#for a wiener process we need normal distribution
#np.random.normal(mean, stdDev);

if __name__=='__main__':
    #assymmetry parameter of the cavity
    x=0.1
    #runtime parameters
    T=4.0 #time
    Nsteps = 1400
    dt = T/Nsteps

    #coupling constants for the systems
    G1 = 1+x
    G2 = 1-x
    g=abs(G1-G2)/(2*x)
    k=5.0*g
    gamma=g**2 / k
    #initialize the RNG from /dev/urandom
    np.random.seed( )
    #create a matrix for the process
    #two sub-systems {0,1},{2,3}
    #matrix for subsystem 1
    SigmaMinus = csr_matrix( np.array( [[0,1], [0,0]]) )
    SigmaPlus = csr_matrix( np.array([[0,0], [1,0]]) )
    
    #create a  the system matrix
    #TODO: automate for N/2 (N) particles
    #subsystem 1
    Subsystem = G1*sp.sparse.kron(SigmaMinus, np.eye(2), 'csr') + G2*sp.sparse.kron(np.eye(2), SigmaMinus, 'csr')
    S1 = sp.sparse.kron(Subsystem, np.eye(4) ,'csr') 
    #subsystem 2
    S2 = sp.sparse.kron(np.eye(4), Subsystem, 'csr')
    print "Init of matrices done"
#
#    #6 atoms
#    S1 = sp.sparse.kron( SigmaMinus, np.eye(4) ) + sp.sparse.kron( sp.sparse.kron( np.eye(2), SigmaMinus), np.eye(2) ) + \
#    sp.sparse.kron( np.eye(4), SigmaMinus )
#    S1 = sp.sparse.kron(S1, np.eye(8) )
#
#    S2 = sp.sparse.kron(SigmaMinus, np.eye(4) ) + sp.sparse.kron( sp.sparse.kron(np.eye(2), SigmaMinus), np.eye(2) ) +\
#    sp.sparse.kron(np.eye(4), SigmaMinus)
#    S2 = sp.sparse.kron( np.eye(8), S2 )
    
    Jminus = S1 + S2
    Jplus = conjugate( transpose(Jminus) )
    
    #create initial state
#    |i>=( |00> + ( |01>-|10>)/sqrt(2) ))/sqrt(2)
#   tensorial 2
#    a = (np.array([0,1,0,0]) - np.array([0,0,1,0])) / np.sqrt(2)
    a = ( G1 *np.array([0,1,0,0]) - G2 * np.array([0,0,1,0]))/np.sqrt( G1**2 + G2**2 )
    iState=(np.array([1,0,0,0])+a) / np.sqrt(2)
    ##initial state for system
    iStateSys = np.tensordot(iState,iState,0).flatten()

#    iStateSys = np.tensordot(iState,iState,0).flatten()
#    iStateSys = np.tensordot(iStateSys, iState, 0).flatten()

    #Data arrays for storing the state of the system
    psiArray = np.zeros( (Nsteps, len(iStateSys)) )
    tArray = np.zeros(Nsteps)
    #repeat numerical experiment
    # Nr of repetitions
    Nrep = 15
    #timing
    tstart = time.time()
    for j in range(Nrep):
        print "Iteration Nr:", j, '\n'
        #EM - state vector
        psi = np.copy(iStateSys)
    
        #heun trivially transferred to stochastic DE
        #for runge-kutta strong order 1
        psiRK = np.copy(iStateSys)
    
        #wiener increment array    
        dW = np.zeros( len(iStateSys) )
        stdDev = np.sqrt(dt)
    
        #propagation of the SDE
        #seed the RNG
    
        for t in range(Nsteps):
            #wiener increment
            dW = np.random.normal(np.zeros(len(dW) ), stdDev)
            #runge-kutta
            psiRK = psiRK +F1(gamma, Jminus, Jplus, psiRK)*dt+F2(gamma, Jminus, psiRK)*dW +\
            0.5* ( F2(gamma, Jminus, psiRK + F2(gamma, Jminus, psiRK)*np.sqrt(dt) ) -\
            F2(gamma, Jminus, psiRK))*(dW**2 - dt)/np.sqrt(dt)
            #store into array
            psiArray[t] = psiRK
            tArray[t]=t*dt
        

    
        #determine <n_ph>
        PhotDensity=np.zeros(Nsteps)
                        
        for i in range(Nsteps):
            PhotDensity[i]=+Nph(Jminus, Jplus, psiArray, tArray, k, g, i+1, T, dt)

    
    #timing
    tend = time.time()
    print "Runtime for N=", Nrep, "repetitions: ", tend-tstart, " sec"
    #analytic result
    tCont = np.linspace(0, T)
    PhotDensity/=Nrep
    #plot
    plt.figure()
    plt.plot(tArray, PhotDensity/(x**2), 'r-')
    plt.xlabel("time in units of gt")
    plt.ylabel("$<n_{ph}(t)>/x^2$")
    plt.grid()
    plt.show()