import numpy as np
import scipy as sp
from scipy import sparse
import time
from scipy.linalg import kron
from matplotlib import pyplot as plt

def F1(a, A, B, x):
    z = np.dot( x, np.dot(A, x) )
    y = a*(2.0*z*np.dot(A,x) - np.dot( np.dot( B , A), x  ) -z*z * x)
    return y
    
def F2(a, A, x):
     z = np.dot( x, np.dot(A, x) )
     y = np.sqrt(2.0*a)*(np.dot(A,x) - z*x)
     return y
     
def ScalInt(k, t):
    '''computes the scalar part of the integrand of the 
    integral (20) HLS'''
    return k*np.exp(-2*k*t)*(np.exp(k*t) -1)

def Nph(Jm, Jp, psiArr, t, k, g, Nsteps, tMax, dt):
    #determine photon density using eq. 20 from the paper
    #as well as trapezoidal rule for quadrature
    res = 0.0
    
    if Nsteps == 0:
        psi = psiArr[0]
        fa = np.dot( psi, np.dot( np.dot( Jp, Jm ), psi ) )
        res = ScalInt(k,0) * fa * 2.0*(g/k)**2
    else:
        for i in range(1,Nsteps-1):
            psi = psiArr[Nsteps-1 -i]

            fa = np.dot( psi, np.dot( np.dot( Jp, Jm ), psi ) )

            res+= ( ScalInt(k,t[i]) *fa )
        res*=2.0
        #first point
        psi = psiArr[Nsteps-1]
        fa = np.dot( psi, np.dot( np.dot( Jp, Jm ), psi ) )
        res += ScalInt(k, 0) * fa
        #last point
        psi = psiArr[0]
        fa = np.dot( psi, np.dot( np.dot( Jp, Jm ), psi ) )
        res += ScalInt(k, t[Nsteps-1]) * fa
        res *= dt/2.0
        res *= 2.0*(g/k)**2
        
    return res


if __name__=='__main__':
    x=0.1
    T=3.0 #time
    Nsteps = 1000
    dt = T/Nsteps
    G1=1.0+x
    G2=1.0-x
    g=abs( G1-G2 )/(2*x)
    k=5.0*g
    gamma=g**2 / k
    np.random.seed( )
    #create a matrix for the process
    #two sub-systems {0,1},{2,3}
    #matrix for subsystem 1
    SigmaMinus = np.array( [[0,1], [0,0]]);
    SigmaPlus = np.array([[0,0], [1,0]]);
    
    #create a  the system matrix
    #TODO: automate for N/2 (N) particles
        #2 particley
    #S1 = kron(SigmaMinus, np.eye(2) )
    #S2 = kron( np.eye(2), SigmaMinus)
    
    #4 atoms
    #subsystem 1
    #S1 = G1*kron(SigmaMinus, np.eye(2)) + G2*kron(np.eye(2), SigmaMinus)
    #S1 = kron(S1, np.eye(4) )
    ##subsystem 2
    #S2 = G1*kron(SigmaMinus, np.eye(2)) + G2*kron(np.eye(2), SigmaMinus)
    #S2 = kron(np.eye(4), S2)

#    #6 atoms
    Subsystem = G1*kron(SigmaMinus, np.eye(2)) + G2*kron(np.eye(2), SigmaMinus)
    S1 = kron(Subsystem, np.eye(2**4) )
    S2 = kron(np.eye(4), kron(Subsystem, np.eye(4)) )
    S3 = kron( np.eye(2**4), Subsystem )
    
    #8 atoms
#    S1 = kron(SigmaMinus, np.eye(8) ) + kron( kron(np.eye(2), SigmaMinus), np.eye(4) ) +\
#    kron(np.eye(4), kron(SigmaMinus, np.eye(2)) ) + kron(np.eye(8), SigmaMinus )
#    S1 = kron(S1, np.eye(16) )
#
#    S2 = kron(SigmaMinus, np.eye(8) ) + kron( kron(np.eye(2), SigmaMinus), np.eye(4) ) +\
#    kron(np.eye(4), kron(SigmaMinus, np.eye(2)) ) + kron(np.eye(8), SigmaMinus )
#    S2 = kron( np.eye(16), S2 )

    
    Jminus = S1 + S2 + S3
    Jplus = np.conj( np.transpose(Jminus) )
    
    #create initial state
#   tensorial 2
#    a = ( G1*np.array([0,1,0,0]) - G2 * np.array([0,0,1,0]))/np.sqrt( G1**2 + G2**2 )
    a = ( np.array([0,1,0,0]) -  np.array([0,0,1,0]))/np.sqrt(2)
    iState=(np.array([1,0,0,0])+a) / np.sqrt(2)
    ##initial state for system
    iStateSys = np.tensordot(iState,iState,0).flatten()
    iStateSys = np.tensordot(iStateSys, iState, 0).flatten()

    #EM - Time array
    psiArray = np.zeros( (Nsteps, len(iStateSys)) )
#    psiArrayEM = np.zeros( (Nsteps, len(iStateSys)) )
    tArray = np.zeros(Nsteps)
    
    #repeat numerical experiment
    # # repetitions
    Nrep = 20
    #timing
    tstart = time.time()
    #save array for photon density
    PhotDensity=np.zeros(Nsteps)
    
    #----------begin of numerical experiment
    for j in range(Nrep):    
        #EM - state vector
#        psi = np.copy(iStateSys)
    
        #heun trivially transferred to stochastic DE
        #for runge-kutta strong order 1
        psiRK = np.copy(iStateSys)
    
        #wiener increment array    
        dW = np.zeros( len(iStateSys) )
        stdDev = np.sqrt(dt)
    
        #propagation of the SDE
        #seed the RNG
    
        for t in range(Nsteps):
            #wiener increment
            #a vector with independent normally distributed random
            #variables with stdDev = dt
            dW = np.random.normal(np.zeros(len(dW) ), stdDev)
            #psi = psi + F1(gamma, Jminus, psi)*dt + F2(gamma, Jminus, psi)*dW
                
            #runge-kutta
            psiRK = psiRK +F1(gamma, Jminus, Jplus, psiRK)*dt+F2(gamma, Jminus, psiRK)*dW +\
            0.5* ( F2(gamma, Jminus, psiRK + F2(gamma, Jminus, psiRK)*np.sqrt(dt) ) -\
            F2(gamma, Jminus, psiRK))*(dW**2 - dt)/np.sqrt(dt)
            
            NormFactor = np.linalg.norm(psiRK, 2)
            psiRK/=NormFactor
            #store into array
            psiArray[t] = psiRK
            #psiArrayEM[t] = psi
            tArray[t]=t*dt
        
    
        #determine <n_ph> for the current run and add it to the total amount                   
        for i in range(Nsteps):
            PhotDensity[i] += Nph(Jminus, Jplus, psiArray, tArray, k, g, i, T, dt)
    
    #------end of numerical experiments, begin of evaluation
    
    #timing
    tend = time.time()
    print "Runtime for N=", Nrep, "repetitions: ", tend-tstart, " sec"

    PhotDensity/=Nrep
    ##plot
    plt.figure()
    plt.plot(tArray, PhotDensity/(x**2), 'r-')#, tArray, PhotDensityEM/(x**2), 'b-')
    plt.xlabel("time in units of gt")
    plt.ylabel("$<n_{ph}(t)>/x^2$")
    plt.grid()
    plt.show()