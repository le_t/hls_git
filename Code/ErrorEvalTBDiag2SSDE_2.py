#!/home/solid/e/lebedev/Enthought/Canopy_64bit/System/bin/python
import sys
import numpy as np
from numpy.linalg import norm
from matplotlib import pyplot as plt
import os, shutil
from multiprocessing import Pool

def evalScript(N, T, x, PyPath, CppPath):
        Nrep=range(50,350,50)
        Nsteps = range(2001, 406001, 6000)
        AvgErrVsN = np.zeros( len(Nsteps))
        SupErrVsN = np.zeros( len(Nsteps))
        for n in range(len(Nsteps)):        
            err = np.zeros(  Nsteps[n] )
            CppDatafile =""
            
            #we have only one result which we diagonalized...
            PyDatafile = PyPath + "%d_TB/Data_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g_Analytic.npz"%(N, N, T, Nsteps[n], 100, x)
            pyData = np.load(PyDatafile)
            tPy = pyData['t']
            JpmPy = pyData['JpmAnalytic']
            pyData.close()
            #...but multiple which were simulated, we choose Nrep = max
            r=max(Nrep)
            #create the filenames from which to load
            CppDatafile = CppPath + "%d/Data_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g"%(N, N, T, Nsteps[n], r, x)
            #load data
            cppData = np.loadtxt(CppDatafile)
            JpmCpp = cppData[:,1]
            err = abs(JpmPy - JpmCpp)
            #set the error to be at a given time point (t=1.0)
            t = (Nsteps[n]-1)/int(T)
            AvgErrVsN[n] = err[t]
            SupErrVsN[n] = norm( err, np.inf)
            
        #convert number to string
        case = str(N)
        directory = case + "_errors"
        #check if directory exists
        if os.path.isdir(directory) == False:
            #it does not exist - create it
            os.mkdir(directory)
        #change into the directory
        os.chdir(directory)
        #save data
        Fname = "Errors_Data_TBDiag_2SSDE_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g_Analytic_2.npz"%(N, T, Nsteps[n], r, x)
        Savefile = open(Fname, "wb")
        np.savez(Savefile, t=tPy, AvgErr = AvgErrVsN, SupErr = SupErrVsN)
        Savefile.close()
        
        dt = T/np.array(Nsteps)
        #plot
        plt.figure(figsize=(15, 11), dpi=72)
        plt.loglog(dt[::-1], AvgErrVsN, 'r-', dt[::-1], SupErrVsN, 'b-')
        plt.legend(("Mean error", "INF-Norm error"), loc='best')
        plt.grid()
        plt.xlabel("Size of the timestep $dt$")
        plt.title("Evolution of the error for N=%d, $N_{rep}=$%d, x=%.1g"%(N, r, x))
        plt.ylabel("Absolute error")
        plt.savefig("Error_Behaviour_Plot_TBDiag_2SSDE_N%d_T%.2g_Nrep%d_x%.1g_2.svg"%(N, T, r, x))
        plt.close()
        
        #get out of the current subdirectory
        os.chdir("..")
        
        return 0;

def runScript(f, args):
    result = f(*args)
    return result;

#from SDE_TB_Function import SDESim
if __name__=='__main__':
    '''main simulation routine for the SDE'''
    N=[2,4,6]
    T = 10.0

    x=0.1
    
    PyResultsDir = ""
    CppResultsDir = ""
    
    
    while len(sys.argv)>1:
        option = sys.argv[1];   del sys.argv[1];
        if option == '-N':
            N = int(sys.argv[1]);  del sys.argv[1];
        elif option == '-pyr':
            PyResultsDir = sys.argv[1];  del sys.argv[1];
        elif option == '-cppr':
            CppResultsDir = sys.argv[1];  del sys.argv[1];
        else:
            print sys.argv[0], " invalid option ", option
            print "Signature: ", sys.argv[0], "-N <Number of Atoms>"
            sys.exit(1)


    

    #create a pool of workers equivalent to the number of cpus
    pool = Pool();
    
    Tasks = [(evalScript, (n, T, x, PyResultsDir, CppResultsDir)) for n in range(2,8,2)]
    
    results = [pool.apply_async(runScript, k) for k in Tasks]
    
    #sync
    for r in results:
        print "Execution state: ", r.get()
    
    