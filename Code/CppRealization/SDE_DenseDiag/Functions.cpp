#include "Functions.h"
#include <cmath>

arma::vec& F1(const dtype a, arma::mat& A, arma::mat& B, arma::vec& v){
    /*! \brief implements the first function for the SSE
     * IN:
     * a- constant
     * A - J_{-} matrix
     * B - J_{+} matrix
     * v - psi vector
     * OUT:  y - vector*/
    
    //create the output vector
    static arma::vec y( v.n_elem, arma::fill::zeros );
    //create an intermediate vector
    static arma::vec z( v.n_elem, arma::fill::zeros );
    //zero the data
    z.fill(0);
    y.fill(0);
    
    // z is for simplification
    z = A*v;
    
    //scalar intermediate value c = <v, Av>
    dtype c = arma::dot(v, z);
    
    y = a*(2.0*c*z - B*z - c*c* v);
    
    return y;
}

arma::vec& F2(const dtype a, arma::mat& A, arma::vec& v){
    /*!\brief Second functions from the SSE (Wiener part)
     * IN:
     * a - constant
     * A - matrix J_{-}
     * v - vector (psi)
     * OUT:
     * y - vector (psi') */
    //create the output vector
    static arma::vec y( v.n_elem, arma::fill::zeros );
    //helper vector
    static arma::vec z( v.n_elem, arma::fill::zeros );
    
    //fill the vectors
    z.fill(0);
    y.fill(0);
    
    z= A*v;
    
    dtype c = arma::dot(v, z);
    
   y = sqrt(2.0*a)*( z - c*v );
    
    return y;
}

void LindbladOpMatrix(const dtype& g, const arma::mat& A, const arma::mat& B, arma::mat& L){
    /*! \brief Create the matix for the lindblad operator
     *IN:
     * A, B references to J-, J+
     * g - gamma factor
     * L - reference to a matrix where the operator matrix will be stored
     * OUT: L - N^2 x N^2 matrix from Eqn. 19  */
    
    uint N=A.n_rows;
    uint Ntot = N*N;
    
    //temporaty matrices
    arma::mat rho(N,N, arma::fill::zeros);
    arma::mat C(N,N, arma::fill::zeros);
    arma::vec LCol;
    
    L.zeros(Ntot,Ntot);
    
    for(uint row(0); row<N; ++row){
        for(uint col(0); col<N; ++col){
            //clear the matrix
            rho.fill(0);
            //'create' an elementary matrix
            rho(row,col)=1.0;
            //Eq. 19
            C= 2.0*A*rho*B-B*A*rho-rho*B*A;
            C*=g;
            //iterate over the result and copy the data to the 

            
            for(uint i(0); i<N; ++i)
                for(uint j(0); j<N; ++j)
                    L(i*N+j,row*N+col) = C(i,j);
            
        }
    }
}