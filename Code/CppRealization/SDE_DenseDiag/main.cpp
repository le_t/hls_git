/* 
 * File:   main.cpp
 * Author: aquinox
 *
 * Created on 24. April 2014, 23:07
 */

#include <cstdlib>
#include <random>
#include "Functions.h"
#include <complex>
//for plotting purposes
#include <mgl2/mgl.h>
#include <mgl2/fltk.h>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //RNG init
    std::random_device seed;
    std::mt19937_64 MersenneRNG(seed());


    //first we are going to implement the RK-s.o.1 
    //SDE integrator scheme

    //required constants
    unsigned N(2), Nrep(100), Nsteps(2001);
    dtype T(20), x(0.1);
    dtype G1(1 + x), G2(1 - x), g, k, gamma, dt, dW;

    //assign the values
    g = abs(G1 - G2) / (2 * x);
    k = 5 * g;
    gamma = g * g / k;

    dt = T / (Nsteps - 1);

    //initialize the RNG distribution
    std::normal_distribution<> WienerDistr(0, dt);

    //create the matrices
    arma::mat SigmaMinus(2, 2, arma::fill::zeros), SigmaPlus(2, 2, arma::fill::zeros);

    //fill
    SigmaMinus(0, 1) = 1.0;
    SigmaPlus(1, 0) = 1.0;

    //create the subsystem matrices
    arma::mat Id = arma::eye(2, 2);
    arma::mat S1 = arma::kron(SigmaMinus, arma::eye(2, 2));
    arma::mat S2 = arma::kron(arma::eye(2, 2), SigmaMinus);
    S1*=G1;
    S2*=G2;
    arma::mat Jminus = S1 + S2;
    arma::mat Jplus = Jminus.st();
    arma::mat Jpm = Jplus*Jminus;

    //create the initial state

    arma::vec iState(4, arma::fill::zeros);
    arma::vec psi(4, arma::fill::zeros);
    arma::vec tmpPsi(4, arma::fill::zeros);
    arma::vec tmp(4, arma::fill::zeros);
    iState(0) = 1. / sqrt(2.0);
    iState(1) = 1. / 2.;
    iState(2) = -1. / 2.;

    //normalise the vector in the 2-norm
    arma::normalise(iState, 2);

    //create the arrays for the values of <t|J+J-|t> ant t
    dtype* t = new dtype[Nsteps];
    dtype* JpmExpect = new dtype[Nsteps];

    //zero the arrays
    for (unsigned i(0); i < Nsteps; ++i) {
        t[i] = 0;
        JpmExpect[i] = 0;
    }

    //execute Nrep repetitions of the experiment
    for (uint r(0); r < Nrep; ++r) {
        //reinitialize the wave function
        psi = iState;
        tmp= Jpm*psi;
        JpmExpect[0] += arma::dot(psi, tmp);
        t[0] = 0;
        //iterate over the timesteps
        for (uint s(1); s < Nsteps; ++s) {
            t[s] = s*dt;
            //get a random number
            dW = WienerDistr(MersenneRNG);

            //execute one step of the RK-s.o. 1 scheme
            tmpPsi = F2(gamma, Jminus, psi);
            tmp = psi + tmpPsi * sqrt(dt);
            psi = psi + F1(gamma, Jminus, Jplus, psi) * dt + tmpPsi * dW \
 + 0.5 * (F2(gamma, Jminus, tmp) - F2(gamma, Jminus, psi)) *(dW * dW - dt) / sqrt(dt);

            //normalise
            arma::normalise(psi, 2);
            tmp= Jpm*psi;
            JpmExpect[s] += arma::dot(psi, tmp);

        }
    }

    //average over all repetitions
    for (uint j(0); j < Nsteps; ++j)
        JpmExpect[j] /= Nrep;
    
    //create big matrix for diagonalization
    arma::mat L(N*N, N*N, arma::fill::zeros);
    LindbladOpMatrix(gamma,Jminus, Jplus,L);

    //L.print("The Lindblad matrix L is:");
    
    //plot
    mglGraph mglGr;
    mglGr.SetSize(800, 600);
    mglData tPlot, X;
    tPlot.Link(t, Nsteps);
    X.Link(JpmExpect, Nsteps);

    mglGr.LoadFont("termes");

    mglGr.Title("Solutions");
    mglGr.Box("k", false);
    mglGr.Grid();
    mglGr.SetRange('y', X);
    mglGr.SetRange('x', tPlot);

    mglGr.Axis();
    mglGr.Label('y', "X(t)");
    mglGr.Label('x', "time ");
    mglGr.Plot(tPlot, X, "r");

    mglGr.WritePNG("Sol.png", "SSE Solution", false);


    //clean up
    delete[] JpmExpect;
    delete[] t;


    return 0;
}

