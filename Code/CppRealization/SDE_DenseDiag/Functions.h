/* 
 * File:   Functions.h
 * Author: aquinox
 *
 * Created on 24. April 2014, 23:07
 */

#ifndef FUNCTIONS_H
#define	FUNCTIONS_H

#include <armadillo>

typedef double dtype;
typedef unsigned int uint;

arma::vec& F1(const dtype a, arma::mat& A, arma::mat& B, arma::vec& v);

arma::vec& F2(const dtype a, arma::mat& A, arma::vec& v);

void LindbladOpMatrix(const dtype& g, const arma::mat& A, const arma::mat& B, arma::mat& L);
#endif	/* FUNCTIONS_H */

