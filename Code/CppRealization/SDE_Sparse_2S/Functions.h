/* 
 * File:   Functions.h
 * Author: aquinox
 *
 * Created on 30. April 2014, 13:45
 */

#ifndef FUNCTIONS_H
#define	FUNCTIONS_H

#include "Misc.h"
#include <armadillo>
#include <iostream>
#include <fstream>
#include <string>


arma::vec& F1(const dtype a, arma::sp_mat& A, arma::sp_mat& B, arma::vec& v);

arma::vec& F2(const dtype a, arma::sp_mat& A, arma::vec& v);

bool dumpResults(const uint N, std::string Fname, const dtype* t, const dtype* JpmExpect, const dtype* JzExpect);

#endif	/* FUNCTIONS_H */

