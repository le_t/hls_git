/* 
 * File:   Misc.h
 * Author: aquinox
 *
 * Created on 30. April 2014, 13:45
 */

#ifndef MISC_H
#define	MISC_H

#include <cmath>
#define SQU(a) ((a)*(a))

typedef double dtype;
typedef unsigned int uint;

#endif	/* MISC_H */

