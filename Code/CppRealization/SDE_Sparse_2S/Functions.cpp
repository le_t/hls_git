#include "Functions.h"
#include <iomanip>

arma::vec& F1(const dtype a, arma::sp_mat& A, arma::sp_mat& B, arma::vec& v){
    /*! \brief implements the first function for the SSE
     * IN:
     * a- constant
     * A - J_{-} matrix
     * B - J_{+} matrix
     * v - psi vector
     * OUT:  y - vector*/
    
    //create the output vector
    static arma::vec y( v.n_elem, arma::fill::zeros );
    //create an intermediate vector
    static arma::vec z( v.n_elem, arma::fill::zeros );
    //zero the data
    z.fill(0);
    y.fill(0);
    
    // z is for simplification
    z = A*v;
    
    //scalar intermediate value c = <v, Av>
    dtype c = arma::dot(v, z);
    
    y = a*(2.0*c*z - B*z - c*c* v);
    
    return y;
}

arma::vec& F2(const dtype a, arma::sp_mat& A, arma::vec& v){
    /*!\brief Second functions from the SSE (Wiener part)
     * IN:
     * a - constant
     * A - matrix J_{-}
     * v - vector (psi)
     * OUT:
     * y - vector (psi') */
    //create the output vector
    static arma::vec y( v.n_elem, arma::fill::zeros );
    //helper vector
    static arma::vec z( v.n_elem, arma::fill::zeros );
    
    //fill the vectors
    z.fill(0);
    y.fill(0);
    
    z= A*v;
    
    dtype c = arma::dot(v, z);
    
   y = sqrt(2.0*a)*( z - c*v );
    
    return y;
}

bool dumpResults(const uint N, std::string Fname, const dtype* t, const dtype* JpmExpect, const dtype* JzExpect){
    /*! \breif Write data to a text file
     * PRE: N -#of elements in the arrays
     * Fname - name of the file to be written to
     * t - vector of N elements containing timepoints
     * JpmExpect - vector containing  <t|J+J-|t>
     * JzExpect - vector containing <t|Jz|t>
     * POST: bool - success or failure
     * Failure means error opening file */
    bool success(true);
    
    //create the file
    std::ofstream Datafile;
    Datafile.open(Fname.c_str());
    
    if(!Datafile.is_open()){
        //if we can't open the file, abort
        std::cerr<<"Error opening file: "<<Fname<<" for reading. exiting..."<<std::endl;
        success = false;
    }
    else{
        Datafile.precision( 14 );
        Datafile.setf(std::ios::scientific);
        for(uint j(0); j<N; ++j){
            Datafile<<t[j]<<'\t'<<JpmExpect[j]<<'\t'<<JzExpect[j]<<std::endl;
        }
    }
    return success;
}