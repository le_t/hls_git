/* 
 * File:   main.cpp
 * Author: aquinox
 *
 * Created on 30. April 2014, 13:44
 */

#include <cstdlib>
#include <random>
#include "Functions.h"
#include <complex>
//for plotting purposes
#include <mgl2/mgl.h>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //RNG init
    std::random_device seed;
    //std::mt19937_64 MersenneRNG(seed());
    std::mt19937_64 MersenneRNG(1354);

    //filename for saving
    std::string Filename("Savedata.dat");
    std::string Picturename;

    //first we are going to implement the RK-s.o.1 
    //SDE integrator scheme

    //required constants
    unsigned N(4), Nrep(100), Nsteps(2001);
    dtype T(20), x(0.1);
    dtype G1(1 + x), G2(1 - x), g, k, gamma, dt, dW, stdDev;

    //automation part - read the data from standard input
    std::cin >> N >> Nrep >> Nsteps >> T >> x >> Filename;


    //assign the values
    g = abs(G1 - G2) / (2 * x);
    k = 5 * g;
    gamma = g * g / k;

    dt = T / (Nsteps - 1);
    
    stdDev = sqrt(dt);
    //initialize the RNG distribution
    std::normal_distribution<> WienerDistr(0, stdDev);

    //create the matrices
    arma::vec sup(N / 2, arma::fill::zeros), sub(N / 2, arma::fill::zeros);
    arma::vec JzDiag(N / 2 + 1, arma::fill::zeros);

    const dtype ConstN(N / 4.0 * (N / 4.0 + 1));
    dtype NcurrUp = N / 4.0;
    dtype NcurrDown = -1.0 * static_cast<dtype> (N) / 4.0;

    //for J+, J-
    for (uint j(0); j < N / 2; ++j) {
        sup(j) = sqrt(ConstN - NcurrUp * (NcurrUp - 1));
        sub(j) = sqrt(ConstN - NcurrDown * (NcurrDown + 1));
        --NcurrUp;
        ++NcurrDown;
    }

    //for J_z
    NcurrDown = -1.0 * static_cast<dtype> (N) / 4.0;
    for (uint j(0); j < N / 2 + 1; ++j)
        JzDiag(j) = NcurrDown + j;



    //helper matrices
    arma::mat tmpMat(N / 2 + 1, N / 2 + 1, arma::fill::zeros);
    //copy data to matrix
    for (uint j(0); j < N / 2; ++j) {
        tmpMat(j, j + 1) = sup(j);
    }
    arma::sp_mat A(arma::kron(arma::eye(N / 2 + 1, N / 2 + 1), tmpMat));
    arma::sp_mat B(arma::kron(tmpMat, arma::eye(N / 2 + 1, N / 2 + 1)));

    arma::sp_mat Jminus = G1 * B + G2*A;

    //'delete' the temporary matrices A,B, tmpMat
    tmpMat.reset();
    A.reset();
    B.reset();

    //recrete them for Jz
    tmpMat = arma::diagmat(JzDiag);
    A = arma::kron(arma::eye(N / 2 + 1, N / 2 + 1), tmpMat);
    B = arma::kron(tmpMat, arma::eye(N / 2 + 1, N / 2 + 1));

    arma::sp_mat Jz = G1 * B + G2*A;

    //'delete' the temporary matrices A,B, tmpMat
    tmpMat.reset();
    A.reset();
    B.reset();

    //create further matrices
    arma::sp_mat Jplus = Jminus.st();
    arma::sp_mat Jpm = Jplus*Jminus;

    //---------------------------------------

    //create the initial state
    uint vecLength((N / 2 + 1)*(N / 2 + 1));

    arma::vec iState(vecLength, arma::fill::zeros);
    arma::vec psi(vecLength, arma::fill::zeros);
    arma::vec tmpPsi(vecLength, arma::fill::zeros);
    arma::vec tmp(vecLength, arma::fill::zeros);
    iState(vecLength - 1) = 1.0;

    //normalise the vector in the 2-norm
    arma::normalise(iState, 2);

    //create the arrays for the values of <t|J+J-|t> ant t
    dtype* t = new dtype[Nsteps];
    dtype* JpmExpect = new dtype[Nsteps];
    dtype* JzExpect = new dtype[Nsteps];

    //zero the arrays
    for (unsigned i(0); i < Nsteps; ++i) {
        t[i] = 0;
        JpmExpect[i] = 0;
        JzExpect[i] = 0;
    }

    //execute Nrep repetitions of the experiment
    for (uint r(0); r < Nrep; ++r) {
        //reinitialize the wave function
        psi = iState;
        //<t|J+J-|t>
        //tmp = Jpm*psi;
        tmp = Jminus*psi;
        //JpmExpect[0] += arma::dot(psi, tmp);
        JpmExpect[0] += arma::dot(tmp,tmp);
        //<t|Jz|t>
        tmp = Jz*psi;
        JzExpect[0] += arma::dot(psi, tmp);
        t[0] = 0;
        //iterate over the timesteps
        for (uint s(1); s < Nsteps; ++s) {
            t[s] = s*dt;
            //get a random number
            dW = WienerDistr(MersenneRNG);

            //execute one step of the RK-s.o. 1 scheme
            tmpPsi = F2(gamma, Jminus, psi);
            tmp = psi + tmpPsi * sqrt(dt);
            psi = psi + F1(gamma, Jminus, Jplus, psi) * dt + tmpPsi * dW \
 + 0.5 * (F2(gamma, Jminus, tmp) - F2(gamma, Jminus, psi)) *(dW * dW - dt) / sqrt(dt);

            //normalise
            arma::normalise(psi, 2);
            //compute <t|J+J-|t>
            //tmp = Jpm*psi;
            tmp = Jminus*psi;
            //JpmExpect[s] += arma::dot(psi, tmp);
            JpmExpect[s] += arma::dot(tmp,tmp);
            //compute <t|Jz|t>
            tmp = Jz*psi;
            JzExpect[s] += arma::dot(psi, tmp);
        }
    }

    //average over the repetitions
    for (uint j(0); j < Nsteps; ++j) {
        JpmExpect[j] /= Nrep;
        JzExpect[j] /= Nrep;
    }


    //save to file
    dumpResults(Nsteps, Filename, t, JpmExpect, JzExpect);
/*
    //plot
    mglGraph mglGr;
    mglGr.SetSize(800, 600);
    mglData tPlot, X, Z;
    tPlot.Link(t, Nsteps);
    X.Link(JpmExpect, Nsteps);
    Z.Link(JzExpect, Nsteps);

    mglGr.LoadFont("termes");

    mglGr.Title("Solutions");
    mglGr.Box("k", false);
    mglGr.Grid();
    mglGr.SetRange('y', X);
    mglGr.SetRange('x', tPlot);

    mglGr.Axis();
    mglGr.Label('y', "X(t)");
    mglGr.Label('x', "time ");
    mglGr.Plot(tPlot, X, "r");
    mglGr.Plot(tPlot, Z, "g");
    
    //determine the name of the picture
    Picturename = Filename+".png";

    mglGr.WritePNG(Picturename.c_str(), "SSE Solution", false);

    //Jz.print("Jz matrix:");
*/
    //clean up
    delete[] JzExpect;
    delete[] JpmExpect;
    delete[] t;

    return 0;
}

