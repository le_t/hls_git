#!/home/solid/e/lebedev/Enthought/Canopy_64bit/System/bin/python
import sys
import numpy as np
import os, shutil
from multiprocessing import Pool

def evalScript(Nsteps, N, T, x, state, analytic):
        simScript="/home/solid/e/lebedev/bin/BA/officeRepo/Code/SDE_TB_Function.py -n %d -nsteps %d -t %.1f -x %.1f -s %d -a %d"%(N,Nsteps, T, x,state,analytic);
        pythonDir = "/home/solid/e/lebedev/Enthought/Canopy_64bit/User/bin/python "
        simCmd = pythonDir+simScript
        print simCmd
        failure = os.system(simCmd)
        if failure:
            #error handling the stupid way - say we have an error, then exit
            print "Running simulation failed!"
            print "parameters used: N=", N, "Nsteps=", Nsteps, "T=",T,"x=",x, "s=",state, "a=",analytic
            sys.exit(1)
        
        return failure;

def runScript(f, args):
    result = f(*args)
    return result;

#from SDE_TB_Function import SDESim
if __name__=='__main__':
    '''main simulation routine for the SDE'''
    N=[2,4,6]
    T = 10.0
    Nsteps = range(2001, 406001, 6000)
    x=0.1
    #initial state 0 - |phi>, 1 - |11>
    state=1
    #flag for "diagonalization only"
    analytic = 0
    
    while len(sys.argv)>1:
        option = sys.argv[1];   del sys.argv[1];
        if option == '-N':
            N = int(sys.argv[1]);  del sys.argv[1];
        else:
            print sys.argv[0], " invalid option ", option
            print "Signature: ", sys.argv[0], "-N <Number of Atoms>"
            sys.exit(1)


    

    #convert number to string
    case = str(N)
    directory = case + "_TB"
    #check if directory exists
    if os.path.isdir(directory):
        #it exists - delete it
        shutil.rmtree(directory)
    #create the directory
    os.mkdir(directory)
    #change into the directory
    os.chdir(directory)

    Nrep = 1
    #create a pool of workers equivalent to the number of cpus
    pool = Pool();
    
    Tasks = [(evalScript, (s, N, T, x,state,analytic)) for s in range(2001, 406001, 6000)]
    
    #for s in Nsteps:
        #create command to run
        #set the path to the file
        #simScript="/home/solid/e/lebedev/bin/BA/Code/SDE_TB_Function.py -n %d -nsteps %d -t %.1f -x %.1f -s %d -a %d"%(N,s, T, x,state,analytic);
        #pythonDir = "/home/solid/e/lebedev/Enthought/Canopy_64bit/User/bin/python "
        #simCmd = pythonDir+simScript
        #print simCmd
        #failure = os.system(simCmd)
        #if failure:
        #    #error handling the stupid way - say we have an error, then exit
        #    print "Running simulation failed!"
        #    print "parameters used: N=", N, "Nsteps=", s, "T=",T,"x=",x, "s=",state, "a=",analytic
        #    sys.exit(1)
    results = [pool.apply_async(runScript, k) for k in Tasks]
    
    #sync
    for r in results:
        print "Execution state: ", r.get()
    
    
    #simulation done
    #change out of the current N-directory into the directory above
    os.chdir("..")