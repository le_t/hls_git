#!/home/solid/e/lebedev/Enthought/Canopy_64bit/System/bin/python
import numpy as np
import scipy as sp
import time
from scipy import sparse
from scipy.sparse import linalg
from scipy.sparse import csr_matrix, eye, kron
from matplotlib import pyplot as plt


import sys

#external file contains the functions
#functions for the SDE
def F1(a, A, B, x):
    z = np.dot( x, A.dot(x) )
    y = a*(2.0*z*A.dot(x) -   (B.dot(A)).dot(x) -z*z * x)
    return y
    
def F2(a, A, x):
     z = np.dot( x, A.dot( x) )
     y = np.sqrt(2.0*a)*(A.dot(x) - z*x)
     return y

#-------------------------------------------

def JPJMApprox(t, x, N, gamma):
    a=x**2 * 0.5*(N+N**2)
    b= -1.0 * gamma *(x**2+x**4)*(3.0*N**2+N**3)
    res = a* np.exp(b*t / a)
    return res


#create the matrix for simulation of eqn. 19


def CreateMatrix(Jminus):
    N= np.shape(Jminus)[0]
    Jflat = (np.array(Jminus.todense())).flatten()
    #create a new empty matrix
    Ntot = N**2
    A= np.zeros( (Ntot, Ntot) )
    
    #fill the matrix
    for row in range(N):
        for col in range(N):
            linIdx = N * row+col
            
            #first part
            for k in range(N):
                for i in range(N):
                    linIdx2=N * k+i
                    #A[linIdx][linIdx2] = 2* Jflat[N*col + row] * Jflat[N*row+col]
                    A[linIdx][linIdx2] += 2*Jflat[N*row+k]*Jflat[N*col+i]

            #second part
            for k in range(N):
                tmp=0
                linIdx2 = N*row+k
                for i in range(N):
                    tmp+= Jflat[N*i+k]*Jflat[N*i+col]
                A[linIdx][linIdx2] -= tmp
            
            #third part
            for i in range(N):
                tmp=0
                linIdx2=N*i+col
                for k in range(N):
                    tmp+=Jflat[N*k+row]*Jflat[N*k+i]
                A[linIdx][linIdx2] -= tmp   
    return csr_matrix(A)

#main runtime
if __name__=='__main__':
    #runtime parameters
    s=1
#    random.seed(125764)
    N = 4
    T=10.0
    #18.02.2013 2300 changed
    #due to having N intervals(steps) is equivalent to N+1 boundary
    #points 
    Nsteps= 2001
    #-----------------------
    Nrep = 300
    x=0.1
    G1=1+x
    G2=1-x
    g=abs(G1-G2)/(2*x)
    k=5*g
    gamma=g**2/k
    
    #still we want each interval to be T/2000 long => N+1->N+1-1=N
    dt=T/(Nsteps-1)
    
    #environment parameters
    np.random.seed(1354)#init the RNG ALLWAYS WITH THE SAME SEQUENCE

#-------------------------------------------------------------------------------
# Section with definitions for the system

    #create the required matrices
    SigmaMinus=csr_matrix( np.array([[0,0],[1,0]]) )
    Subsystem = G1*kron(SigmaMinus, sparse.eye(2)) + G2*kron(sparse.eye(2), SigmaMinus)
    
    ##2 atoms
    if N==2:
        S1 = G1*kron(SigmaMinus, sparse.eye(2) )
        S2 = G2*kron(sparse.eye(2), SigmaMinus )
        Jminus = S1 + S2
    #4 atoms
    elif N==4:
        #subsystem 1
        S1 = G1*kron(SigmaMinus, sparse.eye(2)) + G2*kron(sparse.eye(2), SigmaMinus)
        S1 = kron(S1, sparse.eye(4) )
        #subsystem 2
        S2 = G1*kron(SigmaMinus, sparse.eye(2)) + G2*kron(sparse.eye(2), SigmaMinus)
        S2 = kron(sparse.eye(4), S2)
        Jminus = S1 + S2
    #6 atoms
    elif N==6:
        S1 = kron(Subsystem, sparse.eye(2**4) )
        S2 = kron(sparse.eye(4), kron(Subsystem, sparse.eye(4)) )
        S3 = kron( sparse.eye(2**4), Subsystem )
        Jminus = S1 + S2 + S3

    #8 atoms
    elif N== 8:
        S1 = kron(Subsystem, sparse.eye( 2**(8-2) ))
        S2 = kron(Subsystem, sparse.eye(2**(8-4)))
        S2 = kron(sparse.eye(2**2), S2)
        S3 = kron(sparse.eye(2**(8-4)), Subsystem)
        S3 = kron(S3, sparse.eye(2**2))
        S4 = kron(sparse.eye( 2**(8-2) ), Subsystem)
        Jminus = S1 + S2 + S3 + S4
    #16 atoms
    elif N == 16:
        S1 = kron(Subsystem, sparse.eye( 2**(16-2) ))
        S2 = kron(Subsystem, sparse.eye( 2**(16-4) ))
        S2 = kron( sparse.eye(2**2), S2)
        S3 = kron(Subsystem, sparse.eye( 2**(16-6) ))
        S3 = kron( sparse.eye(2**4), S3)
        S4 = kron(Subsystem, sparse.eye( 2**(16-8) ))
        S4 = kron( sparse.eye( 2**6), S4)
        S5 = kron(sparse.eye( 2**(16-8) ), Subsystem)
        S5 = kron( S5, sparse.eye(2**6) )
        S6 = kron(sparse.eye(2**(16-6) ), Subsystem)
        S6 = kron(S6, sparse.eye(2**4))
        S7 = kron( sparse.eye( 2**(16-4) ), Subsystem)
        S7 = kron( S7, sparse.eye(2**2) )
        S8 = kron( sparse.eye( 2**(16-2)), Subsystem)
        Jminus = S1 + S2 + S3 + S4 + S5 + S6 + S7 + S8
    else :
        print "Error. No preconfigured case for N=",N
        sys.exit(1)
    
    #create the J+ J- matrices
    #Jminus = S1 + S2 + S3# + S4 + S5 + S6 + S7 + S8
    Jplus = (Jminus.transpose()).tocsr()
    Jpm = (Jplus.dot(Jminus)).tocsr()
    
    #create initial state
    #raw for each pair
    if s == 0:
        b = ( np.array([0,0,1,0]) -  np.array([0,1,0,0]) )/np.sqrt(2)
        iState = (np.array([0,0,0,1])+b) / np.sqrt(2)
    elif s == 1:
        iState=np.array([1.0,0.0,0.0,0.0])
    else:
        print "Error. No preconfigured case for inital state index: ", s
        sys.exit(1)
    
    #total
    iStateSys4 = np.tensordot(iState, iState, 0).flatten() #4 atoms
    if N==2:
        iStateSys = iState #2 atoms
    elif N==4:
        iStateSys=iStateSys4
    elif N==6:
        iStateSys = np.tensordot(iStateSys4, iState, 0).flatten() #6 atoms
    elif N>=8:
        iStateSys = np.tensordot(iStateSys4, iStateSys4, 0).flatten() #8 atoms tensorp 4 atoms, 4 atoms
    elif N==16:
        iStateSys = np.tensordot(iStateSys4, iStateSys4, 0).flatten() #8 atoms tensorp 4 atoms, 4 atoms
        iStateSys = np.tensordot(iStateSys, iStateSys, 0).flatten() #16 atoms tensorp 8atoms, 8atoms
    else :
        print "Error. No preconfigured case for N=",N
        sys.exit(1)

    #normalize 
    iNorm = np.linalg.norm(iStateSys, 2)
    iStateSys /= iNorm
#-------------------------------------------------------------------------------  
    #time arrays
    tArray=np.zeros(Nsteps)
    
    
    #auxiliary vector for Milstein
    aux = np.ones( len(iStateSys) )
    b = np.zeros(len(iStateSys))
    #for Milstein method in N-dim with 1-dim Wiener process
    #define in accordance with eq.10.3.2 of [KP1999]
    #an auxiliary matrix C=J-+J-^t
    C = (Jplus + Jminus).tocsr()
    
    #auxiliary vectors for RK-s.o. 3/2 scheme
    EtaP = np.zeros(iStateSys)
    EtaM = np.zeros(iStateSys)
    PhiP = np.zeros(iStateSys)
    PhiM = np.zeros(iStateSys)
    aEp = np.zeros(iStateSys)
    aEm = np.zeros(iStateSys)
    bEp = np.zeros(iStateSys)
    bEm = np.zeros(iStateSys)

    #<t|J+J-|t> - arrays
    JpJmExpect = np.zeros(Nsteps)
    JpJmExpectRK1 = np.zeros(Nsteps)
    #for Milstein comparison
    JpJmExpectM = np.zeros(Nsteps)
    
    
    #timing
    tStart = time.time()
    print "starting iteration"
    for j in range(Nrep):
        
        #initialize the state
        psi = np.copy(iStateSys)
        psiM = np.copy(iStateSys)
        psiRK1 = np.copy(iStateSys)
        stdDev = np.sqrt(dt)
        
        #18.02.204 1300 changed indexing for integration
        #save initial vector into the first position (t=0)
        JpJmExpect[0] += np.dot( psi, Jpm.dot(psi ) )
        JpJmExpectM[0] += np.dot( psiM, Jpm.dot(psiM ) )
        JpJmExpectRK1[0] += np.dot( psiRK1, Jpm.dot(psiRK1 ) )
        tArray[0] = 0.0
        
        #solve using 3/2 scheme and Milstein scheme
        for i in range(1,Nsteps):
            dW1=np.random.normal(0,stdDev)
            dW2=np.random.normal(0,stdDev)
            dZ = dt/2*(dW1+dW2/np.sqrt(3))
            
            #RK s.o. 1 
            psiRK1 = psiRK1 +F1(gamma, Jminus, Jplus, psiRK1)*dt+F2(gamma, Jminus, psiRK1)*dW1 +\
            0.5* ( F2(gamma, Jminus, psiRK1 + F2(gamma, Jminus, psiRK1)*np.sqrt(dt) ) -\
            F2(gamma, Jminus, psiRK1))*(dW1**2 - dt)/np.sqrt(dt)
            
            #Milstein method in N dim
            b = F2(gamma, Jminus, psiM)
            h = F1(gamma, Jminus, Jplus, psiM)
            psiM = psiM + h*dt + b*dW1 + 0.5*(dW1**2-dt) * ( Jminus.dot(b) - np.dot( psiM, Jminus.dot(psiM) )*b +\
            np.dot( b, C.dot(psiM) ) )
            
            
            #RK s.o. 3/2 in N-dim
            
            #step 1 - determine all the random integral varibles
            I00 = dt*dt/2
            I10 = dZ
            I01 = dt*dW1-dZ
            I11 = (dW1*dW1 - dt)/2
            I111 = 0.5*(1.0/3* dW1**2 -dt)*dW1
            #step 2 - determine the auxiliary vectors
            EtaP = psi+F1(gamma, Jminus, Jplus, psi)*dt + F2(gamma, Jminus, psi)*np.sqrt(dt)
            EtaM = psi+F1(gamma, Jminus, Jplus, psi)*dt - F2(gamma, Jminus, psi)*np.sqrt(dt)            
            PhiP = EtaP + F2(gamma, Jminus, EtaP)*np.sqrt(dt)
            PhiM = EtaP - F2(gamma, Jminus, EtaP)*np.sqrt(dt)
            
            #step 3 - determine the vectors used in the scheme (for better readability in the next step
            aEp = F1(gamma, Jminus, Jplus, EtaP)
            aEm = F1(gamma, Jminus, Jplus, EtaM)
            bEp = F2(gamma, Jminus, EtaP)
            bEm = F2(gamma, Jminus, EtaM)
            
            #step 4 - the real RK s.o. 1.5 step
            psi = psi + dt*F1(gamma, Jminus, Jplus, psi) + F2(gamma, Jminus, psi)*dW1 +\
            0.5/np.sqrt(dt) * ( ( aEp - aEm )*I10 + (bEp - bEm)*I11 ) +\
            1.0/(2*dt)*(aEp - 2*F1(gamma, Jminus, Jplus, psi) + aEm)*I00 +\
            1.0/(2*dt)*(bEp - 2*F2(gamma, Jminus, psi) + bEm)*I01 +\
            1.0/(2*dt)*(F2(gamma, Jminus, PhiP) - F2(gamma, Jminus, PhiM)- bEp + bEm)*I111
            
            #normalize s.o 1
            NormFactor = np.linalg.norm(psiRK1, 2)
            psiRK1/= NormFactor
            
            #normalise
            NormFactor = np.linalg.norm(psi, 2)
            psi /= NormFactor
            #normalize milstein
            NormFactor = np.linalg.norm(psiM, 2)
            psiM /= NormFactor
            
        
            #compute <t|J+J-|t> and add it to the array
            JpJmExpect[i] += np.dot( psi, Jpm.dot( psi ) )
            JpJmExpectRK1[i] += np.dot(psiRK1, Jpm.dot( psiRK1 ) )
            JpJmExpectM[i] += np.dot( psiM, Jpm.dot(psiM ) )
            #changed the indexing : i+1 -> i 18.02.2014 1310
            tArray[i] = i*dt


            
    #average
    JpJmExpect /= Nrep
    JpJmExpectRK1 /= Nrep
    JpJmExpectM /= Nrep
    
    
    tEnd = time.time()
    print "Runtime: ", tEnd-tStart, " seconds"
    
    tCont = np.linspace(0, T, Nsteps)
    yCont2 = JPJMApprox(tCont, x, N/2, gamma)
    
    plt.figure(figsize=(15, 11), dpi=72)
    plt.plot(tArray, JpJmExpect, 'b-',  tArray, JpJmExpectM, 'r-', tArray, JpJmExpectRK1, 'g-')
    plt.xlabel("time in units of $gt$")
    plt.ylabel("$\langle J_+ J_{-}(t)\\rangle$")
    plt.legend( ("RK s.o. 1.5", "Milstein s.o. 1", "RK s.o. 1"), loc='upper right' )
    plt.title("Expectation values of $J_+J_-(t)$")
    #plt.xlim(0, 4.0)
    plt.grid()
    #plt.savefig("PhiState_JpJmExpect_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g.png"%(N,T,Nsteps,Nrep,x))
    plt.show()
    #save the data - numerical experiment and diagonalization
    #Fname = "Data_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g_Numerical.npz"%(N,T,Nsteps,Nrep,x)
    #SaveFile = open(Fname, "wb")
    #np.savez(SaveFile, t=tArray, Jpm=JpJmExpectNorm)
    #SaveFile.close()
    
    
    #04.03.2014 - Analytic addenda
    A = gamma * CreateMatrix(Jminus)
    print "Shape of the big matrix: ", np.shape(A)

    #determine eigenvalues and eigenvectors
    #rho_0 - initial density matrix = |phi><phi|- initial states
    rho0 = np.outer(iStateSys, iStateSys.T)
    #flatten
    rho0flat=np.ravel(rho0)
    #compute for every timestep JpJm expectation value
    #using tr( rho J+J-)=<J+J->
    JpJmExpectAnalytic=np.zeros(Nsteps)
    #sparse matrix handling 22.04.14 13:00
    rhotFlat = sp.sparse.linalg.expm_multiply(A, rho0flat, start=0.0, stop=T, num=Nsteps)
    for j in range(Nsteps):
        #determine the new density matrix
        #specifics using sparse matrices
        rhot = csr_matrix( np.reshape( rhotFlat[j], np.shape(Jpm) ) )
        JpJmExpectAnalytic[j] = sum( rhot.dot(Jpm).diagonal() )
    
    for i in range(Nsteps):
        tArray[i] = i*dt    
    
    #save the data - only diagonalization
    #Fname = "Data_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g_Analytic.npz"%(N,T,Nsteps,Nrep,x)
    #SaveFile = open(Fname, "wb")
    #np.savez(SaveFile, t=tArray, JpmAnalytic=JpJmExpectAnalytic)
    #SaveFile.close()
    
    #compute the error
    errRK = JpJmExpect - JpJmExpectAnalytic
    errRK1 = JpJmExpectRK1 - JpJmExpectAnalytic
    errM = JpJmExpectM - JpJmExpectAnalytic
    
    #plot
    plt.figure(figsize=(15, 11), dpi=72)
    plt.plot(tArray, abs(errRK), 'r-', tArray, abs(errM), 'b-', tArray, abs(errRK1), 'g-')
    plt.xlabel("time in units of $gt$")
    plt.ylabel("$\langle J_+ J_{-}(t)\\rangle$")
    plt.legend( ("RK s.o. 1.5", "Milstein s.o. 1", "RK s.o. 1"), loc='upper right' )
    plt.title("Deviations of SDE solutions from diagonalization values of $J_+J_-(t)$")
    #plt.xlim(0, 4.0)
    plt.grid()
    #plt.savefig("PhiState_JpJmExpect_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g.png"%(N,T,Nsteps,Nrep,x))
    plt.show()