import numpy as np
from matplotlib import pyplot as plt

'''Testing routine for the RK-s.o.1 method'''

def func(a, b, t, iV, W):
    '''[KP1999] Eqn 4.6'''
    return iV*np.exp( (a-0.5*b*b)*t+b*W)
    
if __name__=='__main__':
    #define parameters
    a=1.0
    b=0.01
    T=1.0
    Nsteps=2**12
    dt=T/Nsteps
    stdDev = np.sqrt(dt)
    Nrep = 100
    X = np.zeros(Nsteps)
    Xem = np.zeros(Nsteps)
    Xan = np.zeros(Nsteps)
    X0 = 1.0
    Xan[0] = X0
    X[0] = X0
    Xem[0] = X0
    W = np.zeros(Nsteps)
    for s in range(1,Nsteps):
        dW = np.random.normal(0,1)
        X[s] = X[s-1] + a*X[s-1]*dt+b*X[s-1]*dW + 0.5*( b*(X[s-1]+b*X[s-1]*np.sqrt(dt)) -\
        b*X[s-1])*(dW**2-dt)/np.sqrt(dt)
        
        Xem[s] = Xem[s-1]+a*Xem[s-1]*dt + b*Xem[s-1]*dW
        
        W[s] = W[s-1]+dW;
        #psi = psi +F1(gamma, Jminus, Jplus, psi)*dt+F2(gamma, Jminus, psi)*dW +\
        #0.5* ( F2(gamma, Jminus, psi + F2(gamma, Jminus, psi)*np.sqrt(dt) ) -\
        #F2(gamma, Jminus, psi))*(dW**2 - dt)/np.sqrt(dt)
        Xan[s] = func(a,b, s*dt, X0, W[s])
    
    t=np.linspace(0,T, Nsteps)
    
    plt.figure()
    plt.plot(t, X, 'r-', t, Xan, 'g-', t, Xem, 'b-')
    plt.legend(("Numerical RK", "Analytic", "Numerical EM"), loc='best')
    plt.xlabel("time")
    plt.grid()
    plt.show()