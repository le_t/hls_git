#!/home/solid/e/lebedev/Enthought/Canopy_64bit/System/bin/python
import sys
import numpy as np
from numpy.linalg import norm
from matplotlib import pyplot as plt
import os, shutil


#Script to evaluate (and plot) the behaviour of the solution
#of the SSE in comparison to the solution obtained by using
#expm_multiply(matrix 'diagonalization') on the Lindblad-Matrix
#in the 2S basis and tensorbasis respectively

if __name__=='__main__':
    N = [2,4,6]
    Nsteps = range(2001, 406001, 6000)
    Nrep=range(50,350,50)
    T = 10.0
    x = 0.1
    TB2SErrDir =""
    TB2SSDEDir =""
    Fname =""
    
    #get arguments from command line
    while len(sys.argv)>1:
        option = sys.argv[1]
        del sys.argv[1];
        if option=='-tld':
            TB2SErrDir = sys.argv[1];
            TB2SSDEDir = TB2SErrDir;
            del sys.argv[1];
        else:
            #no valid input
            print sys.argv[0], " invalid option ", option
            sys.exit(1)
    
    #create data arrays
    InfNorm1 = np.zeros( (len(Nrep), len(Nsteps)) )
    EuclNorm1 = np.zeros( (len(Nrep), len(Nsteps)) )
    Avg1 = np.zeros( (len(Nrep), len(Nsteps)) )
    InfNorm2 = np.zeros( (len(Nrep), len(Nsteps)) )
    EuclNorm2 = np.zeros( (len(Nrep), len(Nsteps)) )
    Avg2 = np.zeros( (len(Nrep), len(Nsteps)) )
    #we will plot against stepsize rather than step count
    dt = T/np.array(Nsteps)
    #now to fill the arrays
    for n in range(2,8,2):
        # go into an appropriate subdirectory
        #convert number to string
        case = str(n)
        directory = case + "_errors"
        #check if directory exists
        if os.path.isdir(directory) == False:
            #it does not exist, create it
            os.mkdir(directory)
        #change into the directory
        os.chdir(directory)
        #create a subdirectory for the plots
        directory = "ErrorBehaviour"
        if os.path.isdir(directory) == False:
            #it does not exist - create it
            os.mkdir(directory)
        #change into the directory
        os.chdir(directory)
        #now to fill the arrays and plot them
        for s in range( len(Nsteps) ) :
            #load data from the comparison TBDiag - 2S SDE
            Fname = TB2SSDEDir + "%d_errors/SDE_TB/"%(n)
            Fname += "Errors_Data_TBDiag_2SSDE_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g_Analytic.npz"%(n, T, Nsteps[s], 5, x)
            data = np.load(Fname)
            #pick only the relevant data out
            errNrm = data['errorNorms']
            errors = data['errors']
            #close the file
            data.close()
            
            #store the norms of the errors into the arrays InfNorm, EuclNorm, Avg
            #NOTE unsafe if evaluation was done with different # of repetitions
            # everything is hard-wired into the scripts
            InfNorm1[:,s] = np.copy(errNrm[:,1])
            EuclNorm1[:,s] = np.copy(errNrm[:,0])
            Avg1[:,s] = abs( np.copy(errNrm[:,2]) )
            
            #load data from the comparison TBDiag - 2SDiag
            Fname = TB2SErrDir + "%d_errors/TB_2S/"%(n)
            Fname += "Errors_Data_TBDiag_2SDiag_N%d_T%.2g_Nsteps%d_x%.1g_Analytic.npz"%(n, T, Nsteps[s], x)
            data = np.load(Fname)
            errorsDiag = data['errors']
            data.close()
            #compute the difference between the SDE solution and the 2S diag solution
            #errors are additive: errSDE_2S = Jpm_TB - Jpm_SDE - (Jpm_TB - Jpm_2S) = Jpm_2S - Jpm_SDE
            #note again, the sizes are hard-wired and everything may go up in flames
            #if the changes in one file are not propagated into the others!!!
            errors -= errorsDiag
            #compute the norms
            InfNorm2[:,s] = norm(errors, np.inf, axis=1)
            EuclNorm2[:,s] = norm(errors, 2, axis=1)
            Avg2[:,s] = abs( np.average(errors, axis=1) )
        
        #plot the behaviour of the error w.r.t. dt in the 2S basis
        plt.figure(figsize=(15, 11), dpi=72)
        plt.loglog(dt[::-1], np.mean(Avg2, axis=0), 'r-', dt[::-1], dt[::-1], 'b-')
        plt.legend(("Mean eror", "$\mathcal{O}(dt)$"), loc='best')
        plt.xlabel("Timestep size")
        plt.title("Absolute error $| \langle J_+J_-\\rangle_{SDE} - \langle J_+J_-\\rangle_{2S}|$ N=%d"%(n))
        plt.grid()
        plt.savefig("ErrorBehaviourin2SBasis_StochasticMean_N%d_x%.2f.svg"%(n,x))
        plt.close()
        #euclidean norm
        plt.figure(figsize=(15, 11), dpi=72)
        for r in range(len(Nrep)):
            loclabel = "$N_{rep} =$%d"%(Nrep[r])
            plt.semilogy(Nsteps, EuclNorm2[r,:], label=loclabel)
        plt.xlabel("Timestep size")
        plt.title("Absolute error $| \langle J_+J_-\\rangle_{SDE} - \langle J_+J_-\\rangle_{2S}|$ N=%d"%(n))
        plt.legend(loc='best')
        plt.grid()
        plt.savefig("ErrorBehaviourin2SBasis_EuclideanNorm_N%d_x%.2f.svg"%(n,x))
        plt.close()
        #inf norm
        plt.figure(figsize=(15, 11), dpi=72)
        for r in range(len(Nrep)):
            loclabel = "$N_{rep} =$%d"%(Nrep[r])
            plt.semilogy(Nsteps, InfNorm2[r,:], label=loclabel)
        plt.xlabel("Timestep size")
        plt.title("Absolute error $| \langle J_+J_-\\rangle_{SDE} - \langle J_+J_-\\rangle_{2S}|$ N=%d"%(n))
        plt.legend(loc='best')
        plt.grid()
        plt.savefig("ErrorBehaviourin2SBasis_InfNorm_N%d_x%.2f.svg"%(n,x))
        plt.close()
        #average norm
        plt.figure(figsize=(15, 11), dpi=72)
        for r in range(len(Nrep)):
            loclabel = "$N_{rep} =$%d"%(Nrep[r])
            plt.semilogy(Nsteps, Avg2[r,:], label=loclabel)
        plt.xlabel("Timestep size")
        plt.title("Absolute error $| \langle J_+J_-\\rangle_{SDE} - \langle J_+J_-\\rangle_{2S}|$ N=%d"%(n))
        plt.legend(loc='best')
        plt.grid()
        plt.savefig("ErrorBehaviourin2SBasis_Average_N%d_x%.2f.svg"%(n,x))
        plt.close()        
 

        #plot the behaviour of the error w.r.t. dt in the 2S basis for SDE and tensorbasis for diagonalization
        plt.figure(figsize=(15, 11), dpi=72)
        plt.loglog(dt[::-1], np.mean(Avg1, axis=0), 'r-', dt[::-1], dt[::-1], 'b-')
        plt.legend(("Mean eror", "$\mathcal{O}(dt)$"), loc='best')
        plt.xlabel("Timestep size")
        plt.title("Absolute error $| \langle J_+J_-\\rangle_{SDE} - \langle J_+J_-\\rangle_{2S}|$ N=%d"%(n))
        plt.grid()
        plt.savefig("ErrorBehaviourinTPBasis_StochasticMean_N%d_x%.2f.svg"%(n,x))
        plt.close()
        #euclidean norm
        plt.figure(figsize=(15, 11), dpi=72)
        for r in range(len(Nrep)):
            loclabel = "$N_{rep} =$%d"%(Nrep[r])
            plt.semilogy(Nsteps, EuclNorm1[r,:], label=loclabel)
        plt.xlabel("Timestep size")
        plt.title("Absolute error $| \langle J_+J_-\\rangle_{SDE} - \langle J_+J_-\\rangle_{2S}|$ N=%d"%(n))
        plt.legend(loc='best')
        plt.grid()
        plt.savefig("ErrorBehaviourinTPBasis_EuclideanNorm_N%d_x%.2f.svg"%(n,x))
        plt.close()
        #inf norm
        plt.figure(figsize=(15, 11), dpi=72)
        for r in range(len(Nrep)):
            loclabel = "$N_{rep} =$%d"%(Nrep[r])
            plt.semilogy(Nsteps, InfNorm1[r,:], label=loclabel)
        plt.xlabel("Timestep size")
        plt.title("Absolute error $| \langle J_+J_-\\rangle_{SDE} - \langle J_+J_-\\rangle_{2S}|$ N=%d"%(n))
        plt.legend(loc='best')
        plt.grid()
        plt.savefig("ErrorBehaviourinTPBasis_InfNorm_N%d_x%.2f.svg"%(n,x))
        plt.close()
        #average norm
        plt.figure(figsize=(15, 11), dpi=72)
        for r in range(len(Nrep)):
            loclabel = "$N_{rep} =$%d"%(Nrep[r])
            plt.semilogy(Nsteps, Avg1[r,:], label=loclabel)
        plt.xlabel("Timestep size")
        plt.title("Absolute error $| \langle J_+J_-\\rangle_{SDE} - \langle J_+J_-\\rangle_{2S}|$ N=%d"%(n))
        plt.legend(loc='best')
        plt.grid()
        plt.savefig("ErrorBehaviourinTPBasis_Average_N%d_x%.2f.svg"%(n,x))
        plt.close()   
        #get out of the current subdirectory
        os.chdir("..")
        os.chdir("..")       
            