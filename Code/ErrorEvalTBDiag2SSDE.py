#!/home/solid/e/lebedev/Enthought/Canopy_64bit/System/bin/python
import sys
import numpy as np
from numpy.linalg import norm
from matplotlib import pyplot as plt
import os, shutil
from multiprocessing import Pool

def evalScript(Nsteps, N, T, x, PyPath, CppPath):
        Nrep=range(50,350,50)
        
        err = np.zeros( (len(Nrep), Nsteps) )
        errNrm = np.zeros( (len(Nrep), 3) )
        CppDatafile =""
        
        #we have only one result which we diagonalized...
        PyDatafile = PyPath + "Data_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g_Analytic.npz"%(N, T, Nsteps, 100, x)
        pyData = np.load(PyDatafile)
        tPy = pyData['t']
        JpmPy = pyData['JpmAnalytic']
        pyData.close()
        #...but multiple which were simulated
        for r in range( len(Nrep) ):
            #create the filenames from which to load
            CppDatafile = CppPath + "Data_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g"%(N, T, Nsteps, Nrep[r], x)
            #load data
            cppData = np.loadtxt(CppDatafile)
            JpmCpp = cppData[:,1]
            err[r] = abs(JpmPy - JpmCpp)
            #choose ONE timepoint t=1.0
            t = (Nsteps-1)/int(T)
            errNrm[r, 0] = norm( err[r], 2)
            errNrm[r, 1] = norm( err[r], np.inf)
            errNrm[r, 2] = err[r,t]
        
        #convert number to string
        case = str(N)
        directory = case + "_errors"
        #check if directory exists
        if os.path.isdir(directory) == False:
            #it exists - delete it
            os.mkdir(directory)
        #change into the directory
        os.chdir(directory)
        #save data
        Fname = "Errors_Data_TBDiag_2SSDE_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g_Analytic.npz"%(N, T, Nsteps, r, x)
        Savefile = open(Fname, "wb")
        np.savez(Savefile, t=tPy, errors = err, errorNorms=errNrm)
        Savefile.close()
        
        #plot
        plt.figure(figsize=(15, 11), dpi=72)
        for r in range( len(Nrep) ):
            loclabel = "N=%d"%(Nrep[r])
            plt.plot(tPy, err[r], label=loclabel)
        plt.legend(loc='best')
        plt.grid()
        plt.xlabel("Time in units of $gt$")
        plt.title("Evolution over time for N=%d,$N_{steps}=$%d, $N_{rep}=$%d, x=%.1g"%(N, Nsteps, r, x))
        plt.ylabel("Absolute error")
        plt.savefig("Errors_Plot_TBDiag_2SSDE__N%d_T%.2g_Nsteps%d_x%.1g.svg"%(N, T, Nsteps, x))
        plt.close()
        
        #get out of the current subdirectory
        os.chdir("..")
        
        return 0;

def runScript(f, args):
    result = f(*args)
    return result;

#from SDE_TB_Function import SDESim
if __name__=='__main__':
    '''main simulation routine for the SDE'''
    N=[2,4,6]
    T = 10.0
    Nsteps = range(2001, 406001, 6000)
    x=0.1
    
    PyResultsDir = ""
    CppResultsDir = ""
    
    
    while len(sys.argv)>1:
        option = sys.argv[1];   del sys.argv[1];
        if option == '-N':
            N = int(sys.argv[1]);  del sys.argv[1];
        elif option == '-pyr':
            PyResultsDir = sys.argv[1];  del sys.argv[1];
        elif option == '-cppr':
            CppResultsDir = sys.argv[1];  del sys.argv[1];
        else:
            print sys.argv[0], " invalid option ", option
            print "Signature: ", sys.argv[0], "-N <Number of Atoms>"
            sys.exit(1)


    

    #create a pool of workers equivalent to the number of cpus
    pool = Pool();
    
    Tasks = [(evalScript, (s, N, T, x, PyResultsDir, CppResultsDir)) for s in range(2001, 406001, 6000)]
    
    results = [pool.apply_async(runScript, k) for k in Tasks]
    
    #sync
    for r in results:
        print "Execution state: ", r.get()
    
    