#!/home/solid/e/lebedev/Enthought/Canopy_64bit/System/bin/python
import sys
import numpy as np
import os, shutil
#from SDE_TB_Function import SDESim
if __name__=='__main__':
    '''main simulation routine for the SDE'''
    N=[2,4,6]
    Nrep=range(50,350,50)
    T = 10.0
    Nsteps = range(2001, 406001, 6000)
    x=0.1
    #initial state 0 - |phi>, 1 - |11>
    state=1
    #flag for "diagonalization only"
    analytic = 1


    
    for k in N:
        #convert number to string
        case = str(k);
        #check if directory exists
        if os.path.isdir(case):
            #it exists - delete it
            shutil.rmtree(case)
        #create the directory
        os.mkdir(case)
        #change into the directory
        os.chdir(case)
        
        for r in Nrep:
            for s in Nsteps:
                #create command to run
                #set the path to the file
                simScript="/home/solid/e/lebedev/bin/BA/Code/SDE_TB_Function.py -n %d -nsteps %d -nrep %d -t %.1f -x %.1f -s %d -a %d"%(k,s, r, T, x,state,analytic);
                pythonDir = "/home/solid/e/lebedev/Enthought/Canopy_64bit/User/bin/python "
                simCmd = pythonDir+simScript
                print simCmd
                failure = os.system(simCmd)
                if failure:
                    #error handling the stupid way - say we have an error, then exit
                    print "Running simulation failed!"
                    print "parameters used: N=", k, "Nrep=", r, "Nsteps=", s, "T=",T,"x=",x, "s=",state, "a=",analytic
                    sys.exit(1)
        
        #simulation done
        #change out of the current N-directory into the directory above
        os.chdir("..")