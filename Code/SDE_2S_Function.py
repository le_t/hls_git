import numpy as np
import scipy as sp
from scipy import sparse
from scipy.sparse import linalg
from scipy.sparse import csr_matrix, eye, kron

import random
import sys
#external file contains the functions
#functions for the SDE
def F1(a, A, B, x):
    z = np.dot( x, A.dot(x) )
    y = a*(2.0*z*A.dot(x) -   (B.dot(A)).dot(x) -z*z * x)
    return y
    
def F2(a, A, x):
     z = np.dot( x, A.dot( x) )
     y = np.sqrt(2.0*a)*(A.dot(x) - z*x)
     return y

#-------------------------------------------

def JPJMApprox(t, x, N, gamma):
    a=x**2 * 0.5*(N+N**2)
    b= -1.0 * gamma *(x**2+x**4)*(3.0*N**2+N**3)
    res = a* np.exp(b*t / a)
    return res

def ScalInt(k, t):
    '''computes the scalar part of the integrand of the 
    integral (20) HLS'''
    return k * np.exp(-2.*k*t)*(np.exp(k*t) -1)
    
def Nph(JpJmExpect, t, tIdx, dt, k, g):
    '''determine photon density using eq. 20 from the paper
    as well as trapezoidal rule for quadrature
    using only the array of computed <t|J+J-|t>'''
    res = 0.0
    #trapezoidal rule

    
    #everything in-between:
    #ranga(N1, N2) = [N1, N2) over integers
    for j in range(1, tIdx):
        fa = JpJmExpect[tIdx - j]
        res += ScalInt(k,t[j])*fa
    res*=2.
    #first point
    fa = JpJmExpect[tIdx]
    res += ScalInt(k, t[0])*fa
    
    #last point
    fa = JpJmExpect[0]
    res += ScalInt(k, t[tIdx])*fa
    
    #final step of trapezoidal rule
    res *= dt/2.
    
    #last step: constant factor
    res *= 2.0*(g/k)**2
        
    return res

#for analytic approximation
def CoeffA(x, N):
    return x**2*(N+N**2)/2.0

def CoeffB(x, g, N):
    return -1.0*g*(3.0*N**2+N**3)*(x**2+x**4)

def AnalyticPhotonDensity(t, x, g, k, N):
    '''approximative analytic solution obtained by
    integrating eq. 20 using eq. 21 as a first order
    approximation for the exponential function'''
    A=CoeffA(x, N)
    gamma=g**2/k
    B=CoeffB(x, gamma, N)
    part1 = ( np.exp(B*t/A) - np.exp(-1.0*k*t) )/(B+k*A)
    part2 = ( np.exp(B*t/A) - np.exp(-2.0*k*t) )/(B+2.0*A*k)
    return 2.0*(g/k)**2 * k  * A**2 * (part1-part2)

def CreateMatrixFromSparse(Jminus):
    N= np.shape(Jminus)[0]
    Jflat = (np.array(Jminus.todense())).flatten()
    #create a new empty matrix
    Ntot = N**2
    A= np.zeros( (Ntot, Ntot) )
    
    #fill the matrix
    for row in range(N):
        for col in range(N):
            linIdx = N * row+col
            
            #first part
            for k in range(N):
                for i in range(N):
                    linIdx2=N * k+i
                    #A[linIdx][linIdx2] = 2* Jflat[N*col + row] * Jflat[N*row+col]
                    A[linIdx][linIdx2] += 2*Jflat[N*row+k]*Jflat[N*col+i]
            #second part
            for k in range(N):
                tmp=0
                linIdx2 = N*row+k
                for i in range(N):
                    tmp+= Jflat[N*i+k]*Jflat[N*i+col]
                A[linIdx][linIdx2] -= tmp
            
            #third part
            for i in range(N):
                tmp=0
                linIdx2=N*i+col
                for k in range(N):
                    tmp+=Jflat[N*k+row]*Jflat[N*k+i]
                A[linIdx][linIdx2] -= tmp   
    return csr_matrix(A)

#main runtime
def SDESim(N, T = 10.0, Nsteps = 2001, Nrep=200, x=0.1, a=0):
    #parameters depending on the input values
    G1=1+x
    G2=1-x
    g=abs(G1-G2)/(2*x)
    k=5*g
    gamma=g**2/k
    
    #still we want each interval to be T/2000 long => N+1->N+1-1=N
    dt=T/(Nsteps-1)
    
    #environment parameters
    #IN DEBUG RUNS ALWAYS USE THE SAME SEED!!!
    np.random.seed(1354)
    
    #create the required matrices
    
    #superdiagonal for J-
    #length of the irst sub/superdiagonal is (N+1)-1
    sup = np.zeros(N/2)
    sub = np.zeros(N/2)
    #an array for Jz
    JzDiag = np.zeros(N/2+1)
    
    #fill the values
    ConstN = N/4. * (N/4.+1) #first factor in the rot of the raising/lowering operators
    #this factor is constant for the maximal state
    
    #fill
    NcurrUp = N/4.0
    NcurrDown = -N/4.0
    for j in range(N/2):
        sup[j] = np.sqrt( ConstN - NcurrUp*(NcurrUp-1) )
        sub[j] = np.sqrt( ConstN - NcurrDown*(NcurrDown+1) )
        NcurrUp -= 1
        NcurrDown += 1
    
    NcurrDown = -N/4.0
    for j in range(N/2+1):
        JzDiag[j] = NcurrDown + j
    
    #create the J+ J- matrices
    Jminus =  G1*kron( sparse.diags(sup,1), sparse.eye(N/2+1) ) + G2*kron( sparse.eye(N/2+1), sparse.diags(sup,1) )
    Jplus = G1*kron( sparse.diags(sub,-1), sparse.eye(N/2+1) ) + G2*kron( sparse.eye(N/2+1), sparse.diags(sub,-1) )
    Jpm = Jplus.dot(Jminus) 
    
    #create Jz matrix
    Jz =  G1*kron( sparse.diags(JzDiag,0), sparse.eye(N/2+1) ) + G2*kron( sparse.eye(N/2+1), sparse.diags(JzDiag,0) )
    Jz = Jz.tocsr()

    #create initial state
    #maximum excited state
    iState = np.zeros((N/2+1)**2 )
    iState[ -1 ]=1.0 

    #normalize 
    iNorm = np.linalg.norm(iState, 2)
    iState /= iNorm
    
    #<t|J+J-|t> - arrays
    #JpJmExpect = np.zeros(Nsteps)
    JpJmExpectNorm = np.zeros(Nsteps)
    JzExpect = np.zeros(Nsteps)
    
    tArray=np.zeros(Nsteps)
    #if only analytic result is required we skip the SDE
    if a == 0:    
        print "starting iteration"
        for j in range(Nrep):
            
            psiNorm = np.copy(iState)
            #create the vector and stdDeviation for Wiener increment
            #dW = np.zeros( len(iState) )
            stdDev = np.sqrt(dt)
            
            #18.02.204 1300 changed indexing for integration
            #save initial vector into the first position (t=0)
            JpJmExpectNorm[0] += np.dot( psiNorm, Jpm.dot(psiNorm ) )
            JzExpect[0] += np.dot( psiNorm, Jz.dot(psiNorm ) )
            tArray[0] = 0.0
            
            #solve using RK-s.o. 1 scheme
            for i in range(1,Nsteps):
                #initialize the wiener-vector
                #dW = np.random.normal(0, stdDev, len(dW))
                #for l in range(len(dW)):
                #    dW[l] = random.gauss(0,stdDev)
                dW=np.random.normal(0,stdDev)
                
                #basically the RK-1 iteration for two different versions of the state vector
                #one unnormalised the second normalised
                
                psiNorm = psiNorm +F1(gamma, Jminus, Jplus, psiNorm)*dt+F2(gamma, Jminus, psiNorm)*dW +\
                0.5* ( F2(gamma, Jminus, psiNorm + F2(gamma, Jminus, psiNorm)*np.sqrt(dt) ) -\
                F2(gamma, Jminus, psiNorm))*(dW**2 - dt)/np.sqrt(dt)
                
                
                #normalise
                NormFactor = np.linalg.norm(psiNorm, 2)
                psiNorm /= NormFactor
                
                #compute <t|J+J-|t> and add it to the array
                JpJmExpectNorm[i] += np.dot( psiNorm, Jpm.dot( psiNorm ) )
                #compute <t|Jz|t>
                JzExpect[i] += np.dot( psiNorm, Jz.dot( psiNorm ) )
                #changed the indexing : i+1 -> i 18.02.2014 1310
                tArray[i] = i*dt
    
    
                
        #average
        JpJmExpectNorm /= Nrep
        JzExpect /= Nrep
        
        #save to file
        Fname = "Data_2S_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g_Numerical.npz"%(N,T,Nsteps,Nrep,x)
        File = open(Fname, "wb")
        np.savez(File, t=tArray, Jpm = JpJmExpectNorm, Jz=JzExpect)
        File.close()
    
    else:
    #04.03.2014 - Analytic addenda
        A = gamma * CreateMatrixFromSparse(Jminus)
        print "Shape of the big matrix: ", np.shape(A)
    
        #determine eigenvalues and eigenvectors
        #rho_0 - initial density matrix = |phi><phi|- initial states
        rho0 = np.outer(iState, iState.T)
        #flatten
        rho0flat=np.ravel(rho0)
        #compute for every timestep JpJm expectation value
        #using tr( rho J+J-)=<J+J->
        JpJmExpectAnalytic=np.zeros(Nsteps)
        JzExpectAnalytic = np.zeros(Nsteps)
        rhotFlat = sp.sparse.linalg.expm_multiply(A, rho0flat.T, start=0.0, stop=T, num=Nsteps)
        for j in range(Nsteps):
            rhot = csr_matrix( np.reshape( rhotFlat[j], np.shape(Jpm) ) )
            JpJmExpectAnalytic[j] = sum( rhot.dot(Jpm).diagonal() )
            JzExpectAnalytic[j] = sum(  rhot.dot(Jz).diagonal() ) 

        #save to file
        Fname = "Data_2S_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g_Analytic.npz"%(N,T,Nsteps,Nrep,x)
        File = open(Fname, "wb")
        np.savez(File, t=tArray, JpmAnalytic=JpJmExpectAnalytic, JzAnalytic=JzExpectAnalytic)
        File.close()


if __name__=='__main__':
    #stand-alone simulation routine
    #default parameters
    N=2
    Nsteps=2001
    Nrep = 100
    x=0.1
    T=20.0
    s=1
    a=1
    #read parameters from command line
    while len(sys.argv)>1:
        option = sys.argv[1]
        del sys.argv[1];
        if option=='-n':
            N = int(sys.argv[1]);  del sys.argv[1];
        elif option=='-nsteps':
            Nsteps = int(sys.argv[1]);  del sys.argv[1];
        elif option == '-nrep':
            Nrep = int(sys.argv[1]);  del sys.argv[1];
        elif option == '-x':
            x = float(sys.argv[1]);  del sys.argv[1];
        elif option == '-t':
            T = float(sys.argv[1]);  del sys.argv[1];
        elif option == '-a':
            a = int(sys.argv[1]);   del sys.argv[1];
        else:
            #no valid input
            print sys.argv[0], " invalid option ", option
            sys.exit(1)
    
    #run the simulation
    SDESim(N, T, Nsteps, Nrep, x, a);