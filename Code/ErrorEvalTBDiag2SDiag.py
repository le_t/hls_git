#!/home/solid/e/lebedev/Enthought/Canopy_64bit/System/bin/python
import sys
import numpy as np
from numpy.linalg import norm
from matplotlib import pyplot as plt
import os, shutil
from multiprocessing import Pool

def evalScript(Nsteps, N, T, x, PyPathTB, PyPath2S, ErrPath):
        #we have only one result which we diagonalized...
        PyDatafileTB = PyPathTB + "Data_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g_Analytic.npz"%(N, T, Nsteps, 100, x)
        PyDatafile2S = PyPath2S + "Data_2S_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g_Analytic.npz"%(N, T, Nsteps, 100, x)
        #load the results in the tensorbasis
        pyData = np.load(PyDatafileTB)
        tPy = pyData['t']
        JpmPyTB = pyData['JpmAnalytic']
        pyData.close()
        #load the results in the 2S basis
        pyData = np.load(PyDatafile2S)
        JpmPy2S = pyData['JpmAnalytic']
        pyData.close()
        
        #compute the deviation between the results in these two bases
        err = abs(JpmPyTB - JpmPy2S)
        errNrm2 = norm(err,2)
        errNrmInf = norm(err, np.inf)
        errAvg = np.average(err)
        #convert number to string
        case = str(N)
        directory = case + "_errors"
        #check if directory exists
        if os.path.isdir(directory) == False:
            os.mkdir(directory)
        #change into the directory
        os.chdir(directory)
        directory += "analytic"
        if os.path.isdir(directory) == False:
            os.mkdir(directory)
        #change into the directory
        os.chdir(directory)
        
        #save data
        Fname = "Errors_Data_TBDiag_2SDiag_N%d_T%.2g_Nsteps%d_x%.1g_Analytic.npz"%(N, T, Nsteps, x)
        Savefile = open(Fname, "wb")
        np.savez(Savefile, t=tPy, errors = err, errorNorm2=errNrm2, errorNormInf=errNrmInf, AverageError = errAvg)
        Savefile.close()
        
        #plot
        plt.figure(figsize=(15, 11), dpi=72)
        plt.plot(tPy, err)
        plt.grid()
        plt.xlabel("Time in units of $gt$")
        plt.title("Evolution over time for N=%d,$N_{steps}=$%d, x=%.1g"%(N, Nsteps, x))
        plt.ylabel("Absolute error $\langle J_+J_- \\rangle_{TB} - \langle J_+J_- \\rangle_{2S}$")
        plt.savefig("Errors_Plot_TBDiag_2SDiag_N%d_T%.2g_Nsteps%d_x%.1g.svg"%(N, T, Nsteps, x))
        plt.close()
        
        #get out of the current subdirectory
        os.chdir("..")
        os.chdir("..")
        
        return 0;

def runScript(f, args):
    result = f(*args)
    return result;

#from SDE_TB_Function import SDESim
if __name__=='__main__':
    '''main simulation routine for the SDE'''
    N=[2,4,6]
    T = 10.0
    Nsteps = range(2001, 406001, 6000)
    x=0.1
    
    PyResultsDirTB = ""
    PyResultsDir2S = ""
    ErrorsDir = ""
    
    
    while len(sys.argv)>1:
        option = sys.argv[1];   del sys.argv[1];
        if option == '-N':
            N = int(sys.argv[1]);  del sys.argv[1];
        elif option == '-pyrtb':
            PyResultsDirTB = sys.argv[1];  del sys.argv[1];
        elif option == '-pyr2s':
            PyResultsDir2S = sys.argv[1];  del sys.argv[1];
        elif option == '-err':
            ErrorsDir = sys.argv[1];  del sys.argv[1];
        else:
            print sys.argv[0], " invalid option ", option
            print "Signature: ", sys.argv[0], "-N <Number of Atoms>"
            sys.exit(1)


    

    #create a pool of workers equivalent to the number of cpus
    pool = Pool();
    
    Tasks = [(evalScript, (s, N, T, x, PyResultsDirTB, PyResultsDir2S, ErrorsDir)) for s in range(2001, 406001, 6000)]
    
    results = [pool.apply_async(runScript, k) for k in Tasks]
    
    #sync
    for r in results:
        print "Execution state: ", r.get()
    
    