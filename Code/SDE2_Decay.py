import numpy as np
import scipy as sp
import time
from scipy import sparse
from scipy.sparse import csr_matrix, kron
from matplotlib import pyplot as plt


#external file contains the functions
#functions for the SDE
def F1(a, A, B, x):
    z = np.dot( x, A.dot(x) )
    y = a*(2.0*z*A.dot(x) -   (B.dot(A)).dot(x) -z*z * x)
    return y
    
def F2(a, A, x):
     z = np.dot( x, A.dot( x) )
     y = np.sqrt(2.0*a)*(A.dot(x) - z*x)
     return y

#-------------------------------------------

def JPJMApprox(t, x, N, gamma):
    a=x**2 * 0.5*(N+N**2)
    b= -1.0 * gamma *(x**2+x**4)*(3.0*N**2+N**3)
    res = a* np.exp(b*t / a)
    return res

def ScalInt(k, t):
    '''computes the scalar part of the integrand of the 
    integral (20) HLS'''
    return k * np.exp(-2.*k*t)*(np.exp(k*t) -1)
    
def Nph(JpJmExpect, t, tIdx, dt, k, g):
    '''determine photon density using eq. 20 from the paper
    as well as trapezoidal rule for quadrature
    using only the array of computed <t|J+J-|t>'''
    res = 0.0
    #trapezoidal rule

    
    #everything in-between:
    #ranga(N1, N2) = [N1, N2) over integers
    for j in range(1, tIdx):
        fa = JpJmExpect[tIdx - j]
        res += ScalInt(k,t[j])*fa
    res*=2.
    #first point
    fa = JpJmExpect[tIdx]
    res += ScalInt(k, t[0])*fa
    
    #last point
    fa = JpJmExpect[0]
    res += ScalInt(k, t[tIdx])*fa
    
    #final step of trapezoidal rule
    res *= dt/2.
    
    #last step: constant factor
    res *= 2.0*(g/k)**2
        
    return res

#for analytic approximation
def CoeffA(x, N):
    return x**2*(N+N**2)/2.0

def CoeffB(x, g, N):
    return -1.0*g*(3.0*N**2+N**3)*(x**2+x**4)

def AnalyticPhotonDensity(t, x, g, k, N):
    '''approximative analytic solution obtained by
    integrating eq. 20 using eq. 21 as a first order
    approximation for the exponential function'''
    A=CoeffA(x, N)
    gamma=g**2/k
    B=CoeffB(x, gamma, N)
    part1 = ( np.exp(B*t/A) - np.exp(-1.0*k*t) )/(B+k*A)
    part2 = ( np.exp(B*t/A) - np.exp(-2.0*k*t) )/(B+2.0*A*k)
    return 2.0*(g/k)**2 * k  * A**2 * (part1-part2)

#create the matrix for simulation of eqn. 19
def CreateMatrixFromSparse(Jminus):
    N= np.shape(Jminus)[0]
    Jflat = (np.array(Jminus.todense())).flatten()
    #create a new empty matrix
    Ntot = N**2
    A= np.zeros( (Ntot, Ntot) )
    
    #fill the matrix
    for row in range(N):
        for col in range(N):
            linIdx = N * row+col
            
            #first part
            for k in range(N):
                for i in range(N):
                    linIdx2=N * k+i
                    #A[linIdx][linIdx2] = 2* Jflat[N*col + row] * Jflat[N*row+col]
                    A[linIdx][linIdx2] += 2*Jflat[N*row+k]*Jflat[N*col+i]
            #second part
            for k in range(N):
                tmp=0
                linIdx2 = N*row+k
                for i in range(N):
                    tmp+= Jflat[N*i+k]*Jflat[N*i+col]
                A[linIdx][linIdx2] -= tmp
            
            #third part
            for i in range(N):
                tmp=0
                linIdx2=N*i+col
                for k in range(N):
                    tmp+=Jflat[N*k+row]*Jflat[N*k+i]
                A[linIdx][linIdx2] -= tmp   
    return csr_matrix(A)
    
#--------------------------------------------    
#-----------------------------------------------------------------------------------
#--------------------------------------------
#main runtime
if __name__=='__main__':
    #runtime parameters
    N = 6
    T=20.0
    #18.02.2013 2300 changed
    #due to having N intervals(steps) is equivalent to N+1 boundary
    #points 
    Nsteps= 2001
    #-----------------------
    Nrep = 200
    x=0.1
    G1=1.+x
    G2=1.-x
    g=abs(G1-G2)/(2*x)
    k=5*g
    gamma=g**2/k
    
    #still we want each interval to be T/2000 long => N+1->N+1-1=N
    dt=T/(Nsteps-1)
    
    #environment parameters
    np.random.seed(1354)#init the RNG, seed from /dev/urandom
    print "Creating basic matrices..."

    #create the required matrices
    SigmaMinus=csr_matrix( np.array([[0,1],[0,0]]) )
    SigmaPlus = csr_matrix( np.array([[0,0],[1,0]]) )
    SigmaZ = csr_matrix( np.array( [[1,0] , [0,-1]] ) )

    #2 atoms
#    S1 = G1*kron(SigmaMinus, sparse.eye(2) )
#    S2 = G2*kron(sparse.eye(2), SigmaMinus )
#
#    O1 = G1*kron(SigmaZ, sparse.eye(2) )
#    O2 = G2*kron(sparse.eye(2), SigmaZ )
    #4 atoms
    #subsystem 1
    #S1 = G1*kron(SigmaMinus, sparse.eye(2)) + G2*kron(sparse.eye(2), SigmaMinus)
    #S1 = kron(S1, sparse.eye(4) )
    ##subsystem 2
    #S2 = G1*kron(SigmaMinus, sparse.eye(2)) + G2*kron(sparse.eye(2), SigmaMinus)
    #S2 = kron(sparse.eye(4), S2)
    #
    ##Observable 
    #O1 = G1*kron(SigmaZ, sparse.eye(2)) + G2*kron(sparse.eye(2), SigmaZ)
    #O1 = kron(O1, sparse.eye(4) )
    ##subsystem 2
    #O2 = G1*kron(SigmaZ, sparse.eye(2)) + G2*kron(sparse.eye(2), SigmaZ)
    #O2 = kron(sparse.eye(4), O2)    
    
    
    #6 atoms
    Subsystem = G1*kron(SigmaMinus, sparse.eye(2)) + G2*kron(sparse.eye(2), SigmaMinus)
    Osub = G1*kron(SigmaZ, sparse.eye(2)) + G2*kron(sparse.eye(2), SigmaZ)
    S1 = kron(Subsystem, sparse.eye(2**4) )
    S2 = kron(sparse.eye(4), kron(Subsystem, sparse.eye(4)) )
    S3 = kron( sparse.eye(2**4), Subsystem )

    O1 = kron(Osub, sparse.eye(2**4) )
    O2 = kron(sparse.eye(4), kron(Osub, sparse.eye(4)) )
    O3 = kron( sparse.eye(2**4), Osub )
    #8 atoms
    #S1 = kron(Subsystem, sparse.eye( 2**(8-2) ))
    #S2 = kron(Subsystem, sparse.eye(2**(8-4)))
    #S2 = kron(sparse.eye(2**2), S2)
    #S3 = kron(sparse.eye(2**(8-4)), Subsystem)
    #S3 = kron(S3, sparse.eye(2**2))
    #S4 = kron(sparse.eye( 2**(8-2) ), Subsystem)
    #
    #O1 = kron(Osub, sparse.eye( 2**(8-2) ))
    #O2 = kron(Osub, sparse.eye(2**(8-4)))
    #O2 = kron(sparse.eye(2**2), O2)
    #O3 = kron(sparse.eye(2**(8-4)), Osub)
    #O3 = kron(O3, sparse.eye(2**2))
    #O4 = kron(sparse.eye( 2**(8-2) ), Osub)

    #10 atoms
    #S1 = kron(Subsystem, sparse.eye( 2**(10-2) ) )
    #S2 = kron(Subsystem, sparse.eye( 2**(10-4) ) )
    #S2 = kron(sparse.eye(2**2), S2)
    #S3 = kron(sparse.eye(2**(10-6)), Subsystem)
    #S3 = kron(S3, sparse.eye(2**4))
    #S4 = kron(sparse.eye( 2**(10-4) ), Subsystem)
    #S4 = kron(S4, sparse.eye( 2**2 ) )    
    #S5 = kron(sparse.eye( 2**(10-2)), Subsystem)
    #
    #O1 = kron(Osub, sparse.eye( 2**(10-2) ) )
    #O2 = kron(Osub, sparse.eye( 2**(10-4) ) )
    #O2 = kron(sparse.eye(2**2), O2)
    #O3 = kron(sparse.eye(2**(10-6)), Osub)
    #O3 = kron(O3, sparse.eye(2**4))
    #O4 = kron(sparse.eye( 2**(10-4) ), Osub)
    #O4 = kron(O4, sparse.eye( 2**2 ) )    
    #O5 = kron(sparse.eye( 2**(10-2)), Osub)

    #12 atoms
    #S1 = kron(Subsystem, sparse.eye( 2**(12-2) ) )
    #S2 = kron(Subsystem, sparse.eye( 2**(12-4) ) )
    #S2 = kron(sparse.eye( 2**2), S2)
    #S3 = kron(Subsystem, sparse.eye( 2**(12-6) ) )
    #S3 = kron(sparse.eye( 2**4), S3)
    #S4 = kron(sparse.eye( 2**(12-6) ),Subsystem )
    #S4 = kron(S4, sparse.eye( 2**4))    
    #S5 = kron( sparse.eye( 2**(12-4) ),Subsystem )
    #S5 = kron(S5, sparse.eye(2**2) )
    #S6 = kron(sparse.eye( 2**(12-2) ),Subsystem )
    #
    #O1 = kron(Osub, sparse.eye( 2**(12-2) ) )
    #O2 = kron(Osub, sparse.eye( 2**(12-4) ) )
    #O2 = kron(sparse.eye( 2**2), O2)
    #O3 = kron(Osub, sparse.eye( 2**(12-6) ) )
    #O3 = kron(sparse.eye( 2**4), O3)
    #O4 = kron(sparse.eye( 2**(12-6) ),Osub )
    #O4 = kron(O4, sparse.eye( 2**4))    
    #O5 = kron( sparse.eye( 2**(12-4) ),Osub )
    #O5 = kron(O5, sparse.eye(2**2) )
    #O6 = kron(sparse.eye( 2**(12-2) ),Osub )
    
    #14 atoms
#    S1 = kron(Subsystem, sparse.eye( 2**(14-2) ) )
#    S2 = kron(Subsystem, sparse.eye( 2**(14-4) ) )
#    S2 = kron(sparse.eye( 2**2) , S2)
#    S3 = kron(Subsystem, sparse.eye( 2**(14-6) ) )
#    S3 = kron(sparse.eye( 2**4), S3)
#    S4 = kron(Subsystem, sparse.eye( 2**(14-8) ) )
#    S4 = kron(sparse.eye( 2**6), S4)
#    S5 = kron( sparse.eye( 2**8 ), Subsystem)
#    S5 = kron( S5, sparse.eye( 2**(14-10) ) )
#    S6 = kron( sparse.eye( 2**10 ),Subsystem )
#    S6 = kron(S6, sparse.eye( 2**(14-12) ) )
#    S7 = kron( sparse.eye( 2**(14-2) ),Subsystem )
#
#    O1 = kron(Osub, sparse.eye( 2**(14-2) ) )
#    O2 = kron(Osub, sparse.eye( 2**(14-4) ) )
#    O2 = kron(sparse.eye( 2**2) , O2) 
#    O3 = kron(Osub, sparse.eye( 2**(14-6) ) )
#    O3 = kron(sparse.eye( 2**4), O3)
#    O4 = kron(Osub, sparse.eye( 2**(14-8) ) )
#    O4 = kron(sparse.eye( 2**6), O4)
#    O5 = kron( sparse.eye( 2**8 ), Osub)
#    O5 = kron( O5, sparse.eye( 2**(14-10) ) )
#    O6 = kron( sparse.eye( 2**10 ),Osub )
#    O6 = kron( O6, sparse.eye( 2**(14-12) ) )
#    O7 = kron( sparse.eye( 2**(14-2) ),Osub )

    #16 atoms
    #S1 = kron(Subsystem, sparse.eye( 2**(16-2) ))
    #S2 = kron(Subsystem, sparse.eye( 2**(16-4) ))
    #S2 = kron( sparse.eye(2**2), S2)
    #S3 = kron(Subsystem, sparse.eye( 2**(16-6) ))
    #S3 = kron( sparse.eye(2**4), S3)
    #S4 = kron(Subsystem, sparse.eye( 2**(16-8) ))
    #S4 = kron( sparse.eye( 2**6), S4)
    #S5 = kron(sparse.eye( 2**(16-8) ), Subsystem)
    #S5 = kron( S5, sparse.eye(2**6) )
    #S6 = kron(sparse.eye(2**(16-6) ), Subsystem)
    #S6 = kron(S6, sparse.eye(2**4))
    #S7 = kron( sparse.eye( 2**(16-4) ), Subsystem)
    #S7 = kron( S7, sparse.eye(2**2) )
    #S8 = kron( sparse.eye( 2**(16-2)), Subsystem)    
    #
    #O1 = kron(Osub, sparse.eye( 2**(16-2) ))
    #O2 = kron(Osub, sparse.eye( 2**(16-4) ))
    #O2 = kron( sparse.eye(2**2), O2)
    #O3 = kron(Osub, sparse.eye( 2**(16-6) ))
    #O3 = kron( sparse.eye(2**4), O3)
    #O4 = kron(Osub, sparse.eye( 2**(16-8) ))
    #O4 = kron( sparse.eye( 2**6), O4)
    #O5 = kron(sparse.eye( 2**(16-8) ), Osub)
    #O5 = kron( O5, sparse.eye(2**6) )
    #O6 = kron(sparse.eye(2**(16-6) ), Osub)
    #O6 = kron( O6, sparse.eye(2**4))
    #O7 = kron( sparse.eye( 2**(16-4) ), Osub)
    #O7 = kron( O7, sparse.eye(2**2) )
    #O8 = kron( sparse.eye( 2**(16-2)), Osub) 
    
    #create the J+ J- matrices
    print "Creating J+J-, and Observable matrices "
    Jminus = S1 + S2 + S3# + S4# + S5#+ S6 + S7 + S8 
    Jplus = (Jminus.transpose()).tocsr()
    Jpm = (Jplus.dot(Jminus)).tocsr()
    Observable = (O1 + O2+ O3).tocsr()#+ O4#+ O5).tocsr()#+ O6 +O7 + O8).tocsr()
    
    #create initial state    
    #create initial state for a fully excited pair
    iState= np.array([0, 0, 0, 1])
    
    #total
#    iStateSys = iState #2 atoms
#    iStateSys = np.tensordot(iState, iState, 0).flatten() #4 atoms
    iStateSys4 = np.tensordot(iState, iState, 0).flatten() #4 atoms
    iStateSys = np.tensordot(iStateSys4, iState, 0).flatten() #6 atoms
#    iStateSys = np.tensordot(iStateSys4, iStateSys4, 0).flatten() #8 atoms tensorp 4 atoms, 4 atoms
#    iStateSys = np.tensordot(iStateSys, iStateSys4, 0).flatten() #10 atoms tensorp 6atoms, 4 atoms
#    iStateSys = np.tensordot(iStateSys, iStateSys4, 0).flatten() #12 atoms tensorp 8atoms, 4 atoms
#    iStateSys = np.tensordot( iStateSys, iStateSys4, 0).flatten() #14 atoms tensorp 10atoms, 4 atoms
#    iStateSys = np.tensordot(iStateSys, iStateSys, 0).flatten() #16 atoms tensorp 8atoms, 8atoms
    #normalize 
    iNorm = np.linalg.norm(iStateSys, 2)
    iStateSys /= iNorm
    
    #<t|J+J-|t> - arrays
    JpJmExpectNorm = np.zeros(Nsteps)
    JzExpect = np.zeros(Nsteps)
    
    tArray=np.zeros(Nsteps)
    
    #timing
    tStart = time.time()
    #photon density
    photonDensity = np.zeros(Nsteps)

    print "starting iteration"
    for j in range(Nrep):
        
        #initialize the state
        psi = np.copy(iStateSys)
        psiNorm = np.copy(iStateSys)
        #create the vector and stdDeviation for Wiener increment
        #dW = np.zeros( len(iStateSys) )
        stdDev = np.sqrt(dt)
        
        #18.02.204 1300 changed indexing for integration
        #save initial vector into the first position (t=0)

        JpJmExpectNorm[0] += np.dot( psiNorm, Jpm.dot(psiNorm ) )
        JzExpect[0] += np.dot(psiNorm, Observable.dot( psiNorm) )
        tArray[0] = 0.0
        
        #solve using RK-s.o. 1 scheme
        for i in range(1,Nsteps):
            #initialize the wiener-vector
            dW=np.random.normal(0,stdDev)
            
            #basically the RK-1 iteration for two different versions of the state vector
            #one unnormalised the second normalised

            
            psiNorm = psiNorm +F1(gamma, Jminus, Jplus, psiNorm)*dt+F2(gamma, Jminus, psiNorm)*dW +\
            0.5* ( F2(gamma, Jminus, psiNorm + F2(gamma, Jminus, psiNorm)*np.sqrt(dt) ) -\
            F2(gamma, Jminus, psiNorm))*(dW**2 - dt)/np.sqrt(dt)
            
            
            #normalise
            NormFactor = np.linalg.norm(psiNorm, 2)
            psiNorm /= NormFactor
            

            #compute <t|J_z|t> , <t|J+J-|t> and add it to the array
            JpJmExpectNorm[i] += np.dot( psiNorm, Jpm.dot( psiNorm ) )
            JzExpect[i] +=  np.dot( psiNorm, Observable.dot( psiNorm) ) 
            #changed the indexing : i+1 -> i 18.02.2014 1310
            tArray[i] = i*dt

   
               
    #average
    JpJmExpectNorm /= Nrep
    JzExpect /=Nrep
    for i in range(1,Nsteps):
        #compute the density of photons and add the value to the respective array position
        photonDensity[i] += Nph(JpJmExpectNorm, tArray, i, dt, k, g)      
    
    tEnd = time.time()
    print "Runtime: ", tEnd-tStart, " seconds"
    
    
    plt.figure(figsize=(15, 11), dpi=72)
    plt.plot(tArray, JpJmExpectNorm, 'b-', tArray, JzExpect, 'r-')
    plt.legend(("$\langle J_+J_{-}(t)\\rangle$", "$\langle J_z(t)\\rangle$"), loc='upper right')
    plt.xlabel("time in units of $gt$")
    plt.ylabel("$\langle J_{+}J_{-}(t)\\rangle$")
    plt.title("Expectation values of $J_z(t)$")
    plt.xlim(0, T)
    plt.grid()
    plt.savefig( "Jpm_JzExpect_N%d.png"%(N) )
    plt.show()
    #plt.close()
    
    #save to file
    Fname = "Jpm_Jz_TB_N%d_T%.1f_Nrep%d_NSteps%d.npz"%(N,T,Nrep,Nsteps)
    File = open(Fname, "wb")
    np.savez(File, t=tArray, Jpm = JpJmExpectNorm, Jz=JzExpect)
    File.close()

#--------------------------------
    #addendum for 'analytic' data 22.04.14 10:25
    A = gamma * CreateMatrixFromSparse(Jminus)
   
    #eigendecomposition is replaced by expm_multiply
    #rho_0 - initial density matrix = |phi><phi|- initial states
    rho0 = np.outer(iStateSys, iStateSys.T)
    #rho0 = np.tensordot(iStateSys, iStateSys, 0)
    rho0flat = np.ravel(rho0)
    #compute for every timestep JpJm expectation value
    #using tr( rho J+J-)=<J+J->
    JpJmExpectAnalytic=np.zeros(Nsteps)
    JzExpectAnalytic = np.zeros(Nsteps)

    
    #a loop is replaced by expm_multiply : e^{t_k A}*b
    rhotFlat = sp.sparse.linalg.expm_multiply(A, rho0flat.T, start=0.0, stop=T, num=Nsteps)
    
    for j in range(Nsteps):
        rhot = csr_matrix( np.reshape( rhotFlat[j], np.shape(Jpm) ) )
        JpJmExpectAnalytic[j] = sum( rhot.dot(Jpm).diagonal() )
        JzExpectAnalytic[j] = sum(  rhot.dot(Observable).diagonal() )
    
    #plot again
    plt.figure(figsize=(15, 11), dpi=72)
    plt.plot(tArray, JpJmExpectNorm, 'b+', tArray, JpJmExpectAnalytic, 'b-', tArray, JzExpectAnalytic, 'g-', tArray, JzExpect, 'g+')
    plt.xlabel("time in units of $gt$")

    plt.legend( ("$\langle J_+ J_{-}(t)\\rangle$ experimental", "$\langle J_+ J_{-}(t)\\rangle$ analytic", "$\langle J_z\\rangle_t$ analytic", "$\langle J_z\\rangle_t$ experimental"), loc='upper right' )
    plt.title("Results from numerical experiment and diag. solver of eq. 19")
    plt.xlim(0, 7.0)
    plt.grid()
    plt.show()
    plt.savefig("Analytic_JpJmExpect_JzExpect_N%d_T20_Nsteps2000.png"%(N))
    #plt.close()
    
    
    #save to file
    Fname = "Analytic_Jpm_Jz_TB_N%d_T%.1f_Nrep%d_NSteps%d.npz"%(N,T,Nrep,Nsteps)
    File = open(Fname, "wb")
    np.savez(File, t=tArray, Jpm=JpJmExpectAnalytic, Jz=JzExpectAnalytic)
    File.close()