#!/home/solid/e/lebedev/Enthought/Canopy_64bit/System/bin/python
import numpy as np
import scipy as sp
import time
from scipy import sparse
from scipy.sparse import linalg
from scipy.sparse import csr_matrix, eye, kron
from matplotlib import pyplot as plt

import random
import sys

#external file contains the functions
#functions for the SDE
def F1(a, A, B, x):
    z = np.dot( x, A.dot(x) )
    y = a*(2.0*z*A.dot(x) -   (B.dot(A)).dot(x) -z*z * x)
    return y
    
def F2(a, A, x):
     z = np.dot( x, A.dot( x) )
     y = np.sqrt(2.0*a)*(A.dot(x) - z*x)
     return y

#-------------------------------------------

def JPJMApprox(t, x, N, gamma):
    a=x**2 * 0.5*(N+N**2)
    b= -1.0 * gamma *(x**2+x**4)*(3.0*N**2+N**3)
    res = a* np.exp(b*t / a)
    return res

def ScalInt(k, t):
    '''computes the scalar part of the integrand of the 
    integral (20) HLS'''
    return k * np.exp(-2.*k*t)*(np.exp(k*t) -1)
    
def Nph(JpJmExpect, t, tIdx, dt, k, g):
    '''determine photon density using eq. 20 from the paper
    as well as trapezoidal rule for quadrature
    using only the array of computed <t|J+J-|t>'''
    res = 0.0
    #trapezoidal rule

    
    #everything in-between:
    #ranga(N1, N2) = [N1, N2) over integers
    for j in range(1, tIdx):
        fa = JpJmExpect[tIdx - j]
        res += ScalInt(k,t[j])*fa
    res*=2.
    #first point
    fa = JpJmExpect[tIdx]
    res += ScalInt(k, t[0])*fa
    
    #last point
    fa = JpJmExpect[0]
    res += ScalInt(k, t[tIdx])*fa
    
    #final step of trapezoidal rule
    res *= dt/2.
    
    #last step: constant factor
    res *= 2.0*(g/k)**2
        
    return res

#for analytic approximation
def CoeffA(x, N):
    return x**2*(N+N**2)/2.0

def CoeffB(x, g, N):
    return -1.0*g*(3.0*N**2+N**3)*(x**2+x**4)

def AnalyticPhotonDensity(t, x, g, k, N):
    '''approximative analytic solution obtained by
    integrating eq. 20 using eq. 21 as a first order
    approximation for the exponential function'''
    A=CoeffA(x, N)
    gamma=g**2/k
    B=CoeffB(x, gamma, N)
    part1 = ( np.exp(B*t/A) - np.exp(-1.0*k*t) )/(B+k*A)
    part2 = ( np.exp(B*t/A) - np.exp(-2.0*k*t) )/(B+2.0*A*k)
    return 2.0*(g/k)**2 * k  * A**2 * (part1-part2)

#create the matrix for simulation of eqn. 19


def CreateMatrix(Jminus):
    N= np.shape(Jminus)[0]
    Jflat = (np.array(Jminus.todense())).flatten()
    #create a new empty matrix
    Ntot = N**2
    A= np.zeros( (Ntot, Ntot) )
    
    #fill the matrix
    for row in range(N):
        for col in range(N):
            linIdx = N * row+col
            
            #first part
            for k in range(N):
                for i in range(N):
                    linIdx2=N * k+i
                    #A[linIdx][linIdx2] = 2* Jflat[N*col + row] * Jflat[N*row+col]
                    A[linIdx][linIdx2] += 2*Jflat[N*row+k]*Jflat[N*col+i]

            #second part
            for k in range(N):
                tmp=0
                linIdx2 = N*row+k
                for i in range(N):
                    tmp+= Jflat[N*i+k]*Jflat[N*i+col]
                A[linIdx][linIdx2] -= tmp
            
            #third part
            for i in range(N):
                tmp=0
                linIdx2=N*i+col
                for k in range(N):
                    tmp+=Jflat[N*k+row]*Jflat[N*k+i]
                A[linIdx][linIdx2] -= tmp   
    return csr_matrix(A)

#main runtime
def SDESim(N, T = 10.0, Nsteps = 2001, Nrep=200, x=0.1, s=0, a=0):
    #runtime parameters
#    random.seed(125764)
    #N = 6
    #T=10.0
    #18.02.2013 2300 changed
    #due to having N intervals(steps) is equivalent to N+1 boundary
    #points 
    #Nsteps= 10001
    #-----------------------
    #Nrep = 200
    #x=0.1
    G1=1+x
    G2=1-x
    g=abs(G1-G2)/(2*x)
    k=5*g
    gamma=g**2/k
    
    #still we want each interval to be T/2000 long => N+1->N+1-1=N
    dt=T/(Nsteps-1)
    
    #environment parameters
    np.random.seed(1354)#init the RNG ALLWAYS WITH THE SAME SEQUENCE

    #create the required matrices
    SigmaMinus=csr_matrix( np.array([[0,0],[1,0]]) )
    Subsystem = G1*kron(SigmaMinus, sparse.eye(2)) + G2*kron(sparse.eye(2), SigmaMinus)
    
    ##2 atoms
    if N==2:
        S1 = G1*kron(SigmaMinus, sparse.eye(2) )
        S2 = G2*kron(sparse.eye(2), SigmaMinus )
        Jminus = S1 + S2
    #4 atoms
    elif N==4:
        #subsystem 1
        S1 = G1*kron(SigmaMinus, sparse.eye(2)) + G2*kron(sparse.eye(2), SigmaMinus)
        S1 = kron(S1, sparse.eye(4) )
        #subsystem 2
        S2 = G1*kron(SigmaMinus, sparse.eye(2)) + G2*kron(sparse.eye(2), SigmaMinus)
        S2 = kron(sparse.eye(4), S2)
        Jminus = S1 + S2
    #6 atoms
    elif N==6:
        S1 = kron(Subsystem, sparse.eye(2**4) )
        S2 = kron(sparse.eye(4), kron(Subsystem, sparse.eye(4)) )
        S3 = kron( sparse.eye(2**4), Subsystem )
        Jminus = S1 + S2 + S3

    #8 atoms
    elif N== 8:
        S1 = kron(Subsystem, sparse.eye( 2**(8-2) ))
        S2 = kron(Subsystem, sparse.eye(2**(8-4)))
        S2 = kron(sparse.eye(2**2), S2)
        S3 = kron(sparse.eye(2**(8-4)), Subsystem)
        S3 = kron(S3, sparse.eye(2**2))
        S4 = kron(sparse.eye( 2**(8-2) ), Subsystem)
        Jminus = S1 + S2 + S3 + S4
    #16 atoms
    elif N == 16:
        S1 = kron(Subsystem, sparse.eye( 2**(16-2) ))
        S2 = kron(Subsystem, sparse.eye( 2**(16-4) ))
        S2 = kron( sparse.eye(2**2), S2)
        S3 = kron(Subsystem, sparse.eye( 2**(16-6) ))
        S3 = kron( sparse.eye(2**4), S3)
        S4 = kron(Subsystem, sparse.eye( 2**(16-8) ))
        S4 = kron( sparse.eye( 2**6), S4)
        S5 = kron(sparse.eye( 2**(16-8) ), Subsystem)
        S5 = kron( S5, sparse.eye(2**6) )
        S6 = kron(sparse.eye(2**(16-6) ), Subsystem)
        S6 = kron(S6, sparse.eye(2**4))
        S7 = kron( sparse.eye( 2**(16-4) ), Subsystem)
        S7 = kron( S7, sparse.eye(2**2) )
        S8 = kron( sparse.eye( 2**(16-2)), Subsystem)
        Jminus = S1 + S2 + S3 + S4 + S5 + S6 + S7 + S8
    else :
        print "Error. No preconfigured case for N=",N
        sys.exit(1)
    
    #create the J+ J- matrices
    #Jminus = S1 + S2 + S3# + S4 + S5 + S6 + S7 + S8
    Jplus = (Jminus.transpose()).tocsr()
    Jpm = (Jplus.dot(Jminus)).tocsr()
    
    #create initial state
    #raw for each pair
    if s == 0:
        b = ( np.array([0,0,1,0]) -  np.array([0,1,0,0]) )/np.sqrt(2)
        iState = (np.array([0,0,0,1])+b) / np.sqrt(2)
    elif s == 1:
        iState=np.array([1.0,0.0,0.0,0.0])
    else:
        print "Error. No preconfigured case for inital state index: ", s
        sys.exit(1)

    
    #total
    iStateSys4 = np.tensordot(iState, iState, 0).flatten() #4 atoms
    if N==2:
        iStateSys = iState #2 atoms
    elif N==4:
        iStateSys=iStateSys4
    elif N==6:
        iStateSys = np.tensordot(iStateSys4, iState, 0).flatten() #6 atoms
    elif N>=8:
        iStateSys = np.tensordot(iStateSys4, iStateSys4, 0).flatten() #8 atoms tensorp 4 atoms, 4 atoms
    elif N==16:
        iStateSys = np.tensordot(iStateSys4, iStateSys4, 0).flatten() #8 atoms tensorp 4 atoms, 4 atoms
        iStateSys = np.tensordot(iStateSys, iStateSys, 0).flatten() #16 atoms tensorp 8atoms, 8atoms
    else :
        print "Error. No preconfigured case for N=",N
        sys.exit(1)
    
    #normalize 
    iNorm = np.linalg.norm(iStateSys, 2)
    iStateSys /= iNorm
    
    tArray=np.zeros(Nsteps)
    
    #if only analytic result is required we skip the SDE
    if a == 0:
        #<t|J+J-|t> - arrays
        #JpJmExpect = np.zeros(Nsteps)
        JpJmExpectNorm = np.zeros(Nsteps)
        
        
        #psiArray = np.zeros( (Nsteps, len(iStateSys)) )
        psiArrayNorm = np.zeros( (Nsteps, len(iStateSys)) )
        
        
        #timing
        tStart = time.time()
        #photon density
        photonDensity = np.zeros(Nsteps)
        print "starting iteration"
        for j in range(Nrep):
            
            #initialize the state
            psiNorm = np.copy(iStateSys)
            stdDev = np.sqrt(dt)
            
            #18.02.204 1300 changed indexing for integration
            #save initial vector into the first position (t=0)
            JpJmExpectNorm[0] += np.dot( psiNorm, Jpm.dot(psiNorm ) )
            tArray[0] = 0.0
            
            #solve using RK-s.o. 1 scheme
            for i in range(1,Nsteps):
                #initialize the wiener-vector
                #dW = np.random.normal(0, stdDev, len(dW))
                #for l in range(len(dW)):
                #    dW[l] = random.gauss(0,stdDev)
                dW=np.random.normal(0,stdDev)
                
                #basically the RK-1 iteration for two different versions of the state vector
                #one unnormalised the second normalised
                #psi = psi +F1(gamma, Jminus, Jplus, psi)*dt+F2(gamma, Jminus, psi)*dW +\
                #0.5* ( F2(gamma, Jminus, psi + F2(gamma, Jminus, psi)*np.sqrt(dt) ) -\
                #F2(gamma, Jminus, psi))*(dW**2 - dt)/np.sqrt(dt)
                
                psiNorm = psiNorm +F1(gamma, Jminus, Jplus, psiNorm)*dt+F2(gamma, Jminus, psiNorm)*dW +\
                0.5* ( F2(gamma, Jminus, psiNorm + F2(gamma, Jminus, psiNorm)*np.sqrt(dt) ) -\
                F2(gamma, Jminus, psiNorm))*(dW**2 - dt)/np.sqrt(dt)
                
                
                #normalise
                NormFactor = np.linalg.norm(psiNorm, 2)
                psiNorm /= NormFactor
                
                #psiArray[i] = psi
                psiArrayNorm[i] = psiNorm
                #compute <t|J+J-|t> and add it to the array
                JpJmExpectNorm[i] += np.dot( psiNorm, Jpm.dot( psiNorm ) )
                #changed the indexing : i+1 -> i 18.02.2014 1310
                tArray[i] = i*dt
    
    
                
        #average
        JpJmExpectNorm /= Nrep
        for i in range(1,Nsteps):
            #compute the density of photons and add the value to the respective array position
            photonDensity[i] += Nph(JpJmExpectNorm, tArray, i, dt, k, g)      
        
        tEnd = time.time()
        print "Runtime: ", tEnd-tStart, " seconds"
        
        tCont = np.linspace(0, T, Nsteps)
        yCont2 = JPJMApprox(tCont, x, N/2, gamma)
        nphCont = AnalyticPhotonDensity(tCont, x, g, k, N/2)
        
        plt.figure(figsize=(15, 11), dpi=72)
        plt.plot(tArray, JpJmExpectNorm, 'b-', tCont, yCont2, 'g-')#, tCont, yCont2, 'b-')
        plt.xlabel("time in units of $gt$")
        plt.ylabel("$\langle J_+ J_{-}(t)\\rangle$")
        plt.legend( ("Experimental normalized", "Analytical approx."), loc='upper right' )
        plt.title("Expectation values of $J_+J_-(t)$")
        #plt.xlim(0, 4.0)
        plt.grid()
        plt.savefig("PhiState_JpJmExpect_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g.png"%(N,T,Nsteps,Nrep,x))
        #plt.show()
        #plot photon density
        plt.figure(figsize=(15, 11), dpi=72)
        plt.plot( tArray, photonDensity/x**2, 'b-', tCont, nphCont/x**2, 'r--')
        plt.legend(("experimental", "approx. analytic"), loc='upper right')
        plt.xlabel("time in units of $gt$")
        plt.ylabel("$\langle n_{ph}(t)\\rangle$")
        plt.title("Comparison $\langle n_{ph}\\rangle$ from exp, and eqn. 21->20")
        #plt.xlim(0, 4.0)
        plt.grid()
        plt.savefig("PhiState_PhotonDensity_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g.png"%(N,T,Nsteps,Nrep,x))
        #plt.show()
        #save the data - numerical experiment and diagonalization
        Fname = "Data_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g_Numerical.npz"%(N,T,Nsteps,Nrep,x)
        SaveFile = open(Fname, "wb")
        np.savez(SaveFile, t=tArray, Jpm=JpJmExpectNorm)
        SaveFile.close()
        
    else:   
        #04.03.2014 - Analytic addenda
        A = gamma * CreateMatrix(Jminus)
        print "Shape of the big matrix: ", np.shape(A)
    
        #determine eigenvalues and eigenvectors
        #rho_0 - initial density matrix = |phi><phi|- initial states
        rho0 = np.outer(iStateSys, iStateSys.T)
        #flatten
        rho0flat=np.ravel(rho0)
        #compute for every timestep JpJm expectation value
        #using tr( rho J+J-)=<J+J->
        JpJmExpectAnalytic=np.zeros(Nsteps)
        #sparse matrix handling 22.04.14 13:00
        rhotFlat = sp.sparse.linalg.expm_multiply(A, rho0flat, start=0.0, stop=T, num=Nsteps)
        for j in range(Nsteps):
            #determine the new density matrix
            #specifics using sparse matrices
            rhot = csr_matrix( np.reshape( rhotFlat[j], np.shape(Jpm) ) )
            JpJmExpectAnalytic[j] = sum( rhot.dot(Jpm).diagonal() )
        
        for i in range(Nsteps):
            tArray[i] = i*dt    
        
        #save the data - only diagonalization
        Fname = "Data_N%d_T%.2g_Nsteps%d_Nrep%d_x%.1g_Analytic.npz"%(N,T,Nsteps,Nrep,x)
        SaveFile = open(Fname, "wb")
        np.savez(SaveFile, t=tArray, JpmAnalytic=JpJmExpectAnalytic)
        SaveFile.close()




if __name__=='__main__':
    #stand-alone simulation routine
    #default parameters
    N=2
    Nsteps=2001
    Nrep = 100
    x=0.1
    T=20.0
    s=1
    a=1
    #read parameters from command line
    while len(sys.argv)>1:
        option = sys.argv[1]
        del sys.argv[1];
        if option=='-n':
            N = int(sys.argv[1]);  del sys.argv[1];
        elif option=='-nsteps':
            Nsteps = int(sys.argv[1]);  del sys.argv[1];
        elif option == '-nrep':
            Nrep = int(sys.argv[1]);  del sys.argv[1];
        elif option == '-x':
            x = float(sys.argv[1]);  del sys.argv[1];
        elif option == '-t':
            T = float(sys.argv[1]);  del sys.argv[1];
        elif option == '-s':
            s = int(sys.argv[1]);   del sys.argv[1];
        elif option == '-a':
            a = int(sys.argv[1]);   del sys.argv[1];
        else:
            #no valid input
            print sys.argv[0], " invalid option ", option
            sys.exit(1)
    
    #run the simulation
    SDESim(N, T, Nsteps, Nrep, x, s, a);